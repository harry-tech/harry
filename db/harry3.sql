/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80035
 Source Host           : localhost:3306
 Source Schema         : harry3

 Target Server Type    : MySQL
 Target Server Version : 80035
 File Encoding         : 65001

 Date: 10/01/2025 23:11:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_config
-- ----------------------------
DROP TABLE IF EXISTS `gen_config`;
CREATE TABLE `gen_config`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表名',
  `module_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块名',
  `package_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '包名',
  `business_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '业务名',
  `entity_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实体类名',
  `author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '作者',
  `parent_menu_id` bigint NULL DEFAULT NULL COMMENT '上级菜单ID，对应sys_menu的id ',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tablename`(`table_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成基础配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_config
-- ----------------------------

-- ----------------------------
-- Table structure for gen_field_config
-- ----------------------------
DROP TABLE IF EXISTS `gen_field_config`;
CREATE TABLE `gen_field_config`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `config_id` bigint NOT NULL COMMENT '关联的配置ID',
  `column_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `column_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `column_length` int NULL DEFAULT NULL,
  `field_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字段名称',
  `field_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段类型',
  `field_sort` int NULL DEFAULT NULL COMMENT '字段排序',
  `field_comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段描述',
  `max_length` int NULL DEFAULT NULL,
  `is_required` tinyint(1) NULL DEFAULT NULL COMMENT '是否必填',
  `is_show_in_list` tinyint(1) NULL DEFAULT 0 COMMENT '是否在列表显示',
  `is_show_in_form` tinyint(1) NULL DEFAULT 0 COMMENT '是否在表单显示',
  `is_show_in_query` tinyint(1) NULL DEFAULT 0 COMMENT '是否在查询条件显示',
  `query_type` tinyint NULL DEFAULT NULL COMMENT '查询方式',
  `form_type` tinyint NULL DEFAULT NULL COMMENT '表单类型',
  `dict_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `config_id`(`config_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 117 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成字段配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_field_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `config_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '系统内置: Y N ',
  `config_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '键',
  `config_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '值',
  `config_name` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态， 0：禁用 1：启用',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key_idx`(`config_key` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'Y', 'SYS_CAPTCHA_IMG', 'false', '系统验证码开关', '1', '系统验证码开关', '2024-12-20 16:00:45', '2024-12-20 16:00:45', 1);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `pid` bigint NOT NULL COMMENT '上级部门',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '祖级列表',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '启用状态，0:禁用 1:启用',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 0, '0', 'Harry技术', 0, '1', '2024-12-02 15:35:36', '2024-12-02 15:36:08', 1);
INSERT INTO `sys_dept` VALUES (2, 1, '0,1', '研发部', 0, '1', '2024-12-02 15:35:36', '2024-12-02 15:35:59', 1);
INSERT INTO `sys_dept` VALUES (3, 1, '0,1', '营销部', 1, '1', '2024-12-02 15:35:36', '2024-12-02 15:35:57', 1);
INSERT INTO `sys_dept` VALUES (4, 1, '0,1', '产品部', 1, '1', '2024-12-02 15:35:36', '2024-12-02 15:35:36', 1);
INSERT INTO `sys_dept` VALUES (5, 4, '0,1,4', '产品1部', 1, '1', '2024-12-27 10:41:25', '2024-12-27 10:41:25', 1);
INSERT INTO `sys_dept` VALUES (6, 4, '0,1,4', '产品2部', 1, '1', '2024-12-27 10:41:25', '2024-12-27 10:41:25', 1);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '字典名称',
  `type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '启用状态，0:禁用 1:启用',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '数据字典' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '用户性别', 'sys_user_sex', '1', '用户性别', '2024-12-02 14:51:06', '2024-12-02 14:51:12', 1);
INSERT INTO `sys_dict` VALUES (2, '菜单状态', 'sys_show_hide', '1', '菜单状态', '2024-12-02 14:51:06', '2024-12-02 14:51:14', 1);
INSERT INTO `sys_dict` VALUES (3, '系统开关', 'sys_normal_disable', '1', '系统开关', '2024-12-02 14:51:06', '2024-12-02 14:51:15', 1);
INSERT INTO `sys_dict` VALUES (4, '任务状态', 'sys_job_status', '1', '任务状态', '2024-12-02 14:51:06', '2024-12-02 14:51:16', 1);
INSERT INTO `sys_dict` VALUES (5, '任务分组', 'sys_job_group', '1', '任务分组', '2024-12-02 14:51:06', '2024-12-02 14:51:17', 1);
INSERT INTO `sys_dict` VALUES (6, '系统是否', 'sys_yes_no', '1', '系统是否', '2024-12-02 14:51:06', '2024-12-02 14:51:18', 1);
INSERT INTO `sys_dict` VALUES (7, '通知类型', 'sys_notice_type', '1', '通知类型', '2024-12-02 14:51:06', '2024-12-02 14:51:19', 1);
INSERT INTO `sys_dict` VALUES (8, '通知状态', 'sys_notice_status', '1', '通知状态', '2024-12-02 14:51:06', '2024-12-02 14:51:21', 1);
INSERT INTO `sys_dict` VALUES (9, '操作类型', 'sys_oper_type', '1', '操作类型', '2024-12-02 14:51:06', '2024-12-02 14:51:22', 1);
INSERT INTO `sys_dict` VALUES (10, '系统状态', 'sys_common_status', '1', '系统状态', '2024-12-02 14:51:06', '2024-12-02 14:51:24', 1);
INSERT INTO `sys_dict` VALUES (11, '用户岗位', 'sys_user_post', '1', '用户岗位', '2024-12-02 14:51:06', '2024-12-02 14:51:25', 1);
INSERT INTO `sys_dict` VALUES (12, '菜单类型', 'sys_menu_type', '1', '权限类型：0->目录；1->菜单；2->按钮', '2024-12-02 14:51:06', '2024-12-02 14:51:27', 1);
INSERT INTO `sys_dict` VALUES (14, '桶类型', 'sys_bucket_type', '1', '桶类型 aliyun,minio ', '2025-01-10 11:36:26', '2025-01-10 11:36:26', 1);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '是否默认，Y:是 N:否',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '启用状态，0:禁用 1:启用',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', 'success', 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:51', 1);
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', 'warning', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:52', 1);
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', 'info', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:52', 1);
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '1', 'sys_show_hide', '', 'primary', 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:53', 1);
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '0', 'sys_show_hide', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:54', 1);
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '1', 'sys_normal_disable', '', 'primary', 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:55', 1);
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '0', 'sys_normal_disable', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:55', 1);
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '1', 'sys_job_status', '', 'primary', 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:56', 1);
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:52:58', 1);
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', 'primary', 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:00', 1);
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', 'primary', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:02', 1);
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:05', 1);
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:07', 1);
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:08', 1);
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:10', 1);
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:12', 1);
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:13', 1);
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:16', 1);
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:18', 1);
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:21', 1);
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:22', 1);
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:23', 1);
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:24', 1);
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:26', 1);
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:27', 1);
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:28', 1);
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '1', 'sys_common_status', '', 'primary', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:30', 1);
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '0', 'sys_common_status', '', 'danger', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:32', 1);
INSERT INTO `sys_dict_data` VALUES (29, 1, '董事长', '1', 'sys_user_post', NULL, NULL, 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:33', 1);
INSERT INTO `sys_dict_data` VALUES (30, 2, '项目经理', '2', 'sys_user_post', NULL, NULL, 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:35', 1);
INSERT INTO `sys_dict_data` VALUES (31, 3, '人力资源', '3', 'sys_user_post', NULL, NULL, 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:36', 1);
INSERT INTO `sys_dict_data` VALUES (32, 4, '产品经理', '4', 'sys_user_post', NULL, NULL, 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:38', 1);
INSERT INTO `sys_dict_data` VALUES (33, 5, '普通员工', '5', 'sys_user_post', NULL, NULL, 'Y', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:39', 1);
INSERT INTO `sys_dict_data` VALUES (34, 1, '目录', '0', 'sys_menu_type', NULL, 'primary', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:53:41', 1);
INSERT INTO `sys_dict_data` VALUES (35, 2, '菜单', '1', 'sys_menu_type', NULL, 'success', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:54:55', 1);
INSERT INTO `sys_dict_data` VALUES (36, 3, '按钮', '2', 'sys_menu_type', NULL, 'warning', 'N', '1', '2024-12-02 14:52:18', '2024-12-02 14:54:57', 1);
INSERT INTO `sys_dict_data` VALUES (38, 4, '外链', '4', 'sys_menu_type', NULL, 'primary', 'N', '1', '2024-12-20 12:04:02', '2024-12-20 12:04:02', 1);
INSERT INTO `sys_dict_data` VALUES (39, 1, 'ALIYUN', 'ALIYUN', 'sys_bucket_type', NULL, 'success', 'N', '1', '2025-01-10 11:37:40', '2025-01-10 11:37:40', 1);
INSERT INTO `sys_dict_data` VALUES (40, 1, 'MINIO', 'MINIO', 'sys_bucket_type', NULL, 'warning', 'N', '1', '2025-01-10 11:37:54', '2025-01-10 11:37:54', 1);

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `original` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原始文件名',
  `bucket_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件存储桶名称',
  `bucket_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '桶类型 aliyun,minio',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `file_size` bigint NULL DEFAULT NULL COMMENT '文件大小',
  `domain` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问地址',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `modify_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file
-- ----------------------------

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `method` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '操作类别（0 其它 1 后台用户 2 移动端用户）',
  `username` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `ip` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `location` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `param` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `execution_time` bigint NULL DEFAULT NULL COMMENT '执行时间(ms)',
  `browser` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  `browser_version` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器版本',
  `os` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '终端系统',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `pid` bigint NULL DEFAULT NULL COMMENT '父级菜单id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `permission` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
  `type` int NULL DEFAULT NULL COMMENT '权限类型，0:目录 1:菜单 2:按钮（接口绑定权限）3:外链',
  `uri` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组件路径',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `keep_alive` tinyint NULL DEFAULT NULL COMMENT '【菜单】是否开启页面缓存（1-是 0-否）',
  `always_show` tinyint NULL DEFAULT NULL COMMENT '【目录】只有一个子路由是否始终显示（1-是 0-否）',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路由地址',
  `params` json NULL COMMENT '【菜单】路由参数',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '显示状态，0:隐藏 1:显示',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1054 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', '', 'system', 0, '', 1, NULL, NULL, 'system', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (2, 0, '系统监控', NULL, 'monitor', 0, NULL, 1, 1, 1, 'monitor', '[]', '1', '2025-01-06 16:31:10', 1);
INSERT INTO `sys_menu` VALUES (3, 0, '代码生成', NULL, 'code', 0, '', 1, 1, 1, 'code', '[]', '1', '2025-01-03 14:15:49', 1);
INSERT INTO `sys_menu` VALUES (100, 1, '用户管理', 'sys_user_page', 'user', 1, 'system/user/index', 1, 1, NULL, 'user', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (101, 1, '角色管理', 'sys_role_page', 'role', 1, 'system/role/index', 2, 1, NULL, 'role', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (102, 1, '菜单管理', 'sys_menu_page', 'menu', 1, 'system/menu/index', 3, 1, NULL, 'menu', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (103, 1, '部门管理', 'sys_dept_page', 'tree', 1, 'system/dept/index', 4, 1, NULL, 'dept', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (105, 1, '字典管理', 'sys_dict_page', 'dict', 1, 'system/dict/index', 6, 1, NULL, 'dict', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (106, 1, '字典数据', 'sys_dict_data_page', 'dict-data', 1, 'system/dict/data', 8, 1, NULL, 'dict-data', NULL, '0', '2024-12-20 08:33:50', 1);
INSERT INTO `sys_menu` VALUES (107, 1, '系统配置', 'sys_config_page', 'config', 1, 'system/config/index', 7, 1, NULL, 'config', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (108, 1, '文件管理', NULL, 'download', 1, 'system/file/index', 1, 1, 0, 'file', '[]', '1', '2025-01-10 11:23:19', 1);
INSERT INTO `sys_menu` VALUES (200, 2, '操作日志', 'sys_log_page', 'collapse', 1, 'monitor/syslog/index', 1, 1, NULL, 'syslog', '[]', '1', '2025-01-06 16:33:41', 1);
INSERT INTO `sys_menu` VALUES (300, 3, '代码生成', NULL, NULL, 1, 'codegen/index', 1, 1, 0, 'gen', NULL, '1', '2025-01-03 14:17:37', 1);
INSERT INTO `sys_menu` VALUES (999, 1, '外链', NULL, 'close_other', 3, NULL, 1, 1, 0, 'http://127.0.0.1:9000/doc.html', '[]', '1', '2024-12-20 13:25:46', 1);
INSERT INTO `sys_menu` VALUES (1001, 100, '用户查询', 'sys_user_get', '', 2, '', 1, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1002, 100, '用户新增', 'sys_user_add', '', 2, '', 2, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1003, 100, '用户修改', 'sys_user_edit', '', 2, '', 3, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1004, 100, '用户删除', 'sys_user_del', '', 2, '', 4, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1005, 100, '用户导出', 'sys_user_export', '', 2, '', 5, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1006, 100, '用户导入', 'sys_user_import', '', 2, '', 6, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1007, 100, '重置密码', 'sys_user_reset', '', 2, '', 7, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1008, 101, '角色查询', 'sys_role_get', '', 2, '', 1, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1009, 101, '角色新增', 'sys_role_add', '', 2, '', 2, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1010, 101, '角色修改', 'sys_role_edit', '', 2, '', 3, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1011, 101, '角色删除', 'sys_role_del', '', 2, '', 4, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1012, 101, '角色导出', 'sys_role_export', '', 2, '', 5, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1013, 102, '菜单查询', 'sys_menu_get', '', 2, '', 1, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1014, 102, '菜单新增', 'sys_menu_add', '', 2, '', 2, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1015, 102, '菜单修改', 'sys_menu_edit', '', 2, '', 3, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1016, 102, '菜单删除', 'sys_menu_del', '', 2, '', 4, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1017, 103, '部门查询', 'sys_dept_get', '', 2, '', 1, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1018, 103, '部门新增', 'sys_dept_add', '', 2, '', 2, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1019, 103, '部门修改', 'sys_dept_edit', '', 2, '', 3, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1020, 103, '部门删除', 'sys_dept_del', '', 2, '', 4, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1026, 105, '字典查询', 'sys_dict_get', '', 2, '', 1, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1027, 105, '字典新增', 'sys_dict_add', '', 2, '', 2, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1028, 105, '字典修改', 'sys_dict_edit', '', 2, '', 3, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1029, 105, '字典删除', 'sys_dict_del', '', 2, '', 4, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1030, 105, '字典导出', 'sys_dict_export', '', 2, '', 5, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1031, 107, '系统配置查询', 'sys_config_get', '', 2, '', 1, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1032, 107, '系统配置新增', 'sys_config_add', '', 2, '', 2, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1033, 107, '系统配置修改', 'sys_config_edit', '', 2, '', 3, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1034, 107, '系统配置删除', 'sys_config_del', '', 2, '', 4, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1035, 107, '系统配置导出', 'sys_config_export', '', 2, '', 5, NULL, NULL, '', NULL, '1', '2024-12-02 14:56:50', 1);
INSERT INTO `sys_menu` VALUES (1056, 108, '文件管理查询', 'sys_config_get', '', 2, '', 1, NULL, NULL, NULL, NULL, '1', '2025-01-10 11:27:03', 1);
INSERT INTO `sys_menu` VALUES (1057, 108, '文件管理新增', 'sys_config_add', '', 2, '', 2, NULL, NULL, NULL, NULL, '1', '2025-01-10 11:27:03', 1);
INSERT INTO `sys_menu` VALUES (1058, 108, '文件管理修改', 'sys_config_edit', '', 2, '', 3, NULL, NULL, NULL, NULL, '1', '2025-01-10 11:27:03', 1);
INSERT INTO `sys_menu` VALUES (1059, 108, '文件管理删除', 'sys_config_del', '', 2, '', 4, NULL, NULL, NULL, NULL, '1', '2025-01-10 11:27:03', 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '角色权限字符',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 ）',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `user_count` int NULL DEFAULT NULL COMMENT '后台用户数量',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '启用状态，0:禁用 1:启用',
  `sort` int NULL DEFAULT 0 COMMENT '排序',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'ROOT', '0', '超级管理员', 1, '2024-12-02 10:57:02', '1', 0, 1);
INSERT INTO `sys_role` VALUES (2, '测试', 'TEST', '1', '测试角色', 1, '2024-12-02 15:21:49', '1', 0, 1);
INSERT INTO `sys_role` VALUES (3, 'c角色', 'ADD', '3', NULL, NULL, '2024-12-23 17:41:40', '1', 1, 1);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 203 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和菜单关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (91, 2, 1);
INSERT INTO `sys_role_menu` VALUES (92, 2, 100);
INSERT INTO `sys_role_menu` VALUES (93, 2, 1001);
INSERT INTO `sys_role_menu` VALUES (94, 2, 1002);
INSERT INTO `sys_role_menu` VALUES (95, 2, 1003);
INSERT INTO `sys_role_menu` VALUES (96, 2, 1004);
INSERT INTO `sys_role_menu` VALUES (97, 2, 1005);
INSERT INTO `sys_role_menu` VALUES (98, 2, 1006);
INSERT INTO `sys_role_menu` VALUES (99, 2, 1007);
INSERT INTO `sys_role_menu` VALUES (100, 2, 101);
INSERT INTO `sys_role_menu` VALUES (101, 2, 1008);
INSERT INTO `sys_role_menu` VALUES (102, 2, 1009);
INSERT INTO `sys_role_menu` VALUES (103, 2, 1010);
INSERT INTO `sys_role_menu` VALUES (104, 2, 1011);
INSERT INTO `sys_role_menu` VALUES (105, 2, 1012);
INSERT INTO `sys_role_menu` VALUES (106, 2, 102);
INSERT INTO `sys_role_menu` VALUES (107, 2, 1013);
INSERT INTO `sys_role_menu` VALUES (108, 2, 1014);
INSERT INTO `sys_role_menu` VALUES (109, 2, 1015);
INSERT INTO `sys_role_menu` VALUES (110, 2, 1016);
INSERT INTO `sys_role_menu` VALUES (111, 2, 103);
INSERT INTO `sys_role_menu` VALUES (112, 2, 1017);
INSERT INTO `sys_role_menu` VALUES (113, 2, 1018);
INSERT INTO `sys_role_menu` VALUES (114, 2, 1019);
INSERT INTO `sys_role_menu` VALUES (115, 2, 1020);
INSERT INTO `sys_role_menu` VALUES (116, 2, 105);
INSERT INTO `sys_role_menu` VALUES (117, 2, 1026);
INSERT INTO `sys_role_menu` VALUES (118, 2, 1027);
INSERT INTO `sys_role_menu` VALUES (119, 2, 1028);
INSERT INTO `sys_role_menu` VALUES (120, 2, 1029);
INSERT INTO `sys_role_menu` VALUES (121, 2, 1030);
INSERT INTO `sys_role_menu` VALUES (122, 2, 107);
INSERT INTO `sys_role_menu` VALUES (123, 2, 1031);
INSERT INTO `sys_role_menu` VALUES (124, 2, 1032);
INSERT INTO `sys_role_menu` VALUES (125, 2, 1033);
INSERT INTO `sys_role_menu` VALUES (126, 2, 1034);
INSERT INTO `sys_role_menu` VALUES (127, 2, 1035);
INSERT INTO `sys_role_menu` VALUES (128, 2, 106);
INSERT INTO `sys_role_menu` VALUES (167, 3, 1);
INSERT INTO `sys_role_menu` VALUES (168, 3, 100);
INSERT INTO `sys_role_menu` VALUES (169, 3, 1001);
INSERT INTO `sys_role_menu` VALUES (170, 3, 1002);
INSERT INTO `sys_role_menu` VALUES (171, 3, 1003);
INSERT INTO `sys_role_menu` VALUES (172, 3, 1004);
INSERT INTO `sys_role_menu` VALUES (173, 3, 1005);
INSERT INTO `sys_role_menu` VALUES (174, 3, 1006);
INSERT INTO `sys_role_menu` VALUES (175, 3, 1007);
INSERT INTO `sys_role_menu` VALUES (176, 3, 101);
INSERT INTO `sys_role_menu` VALUES (177, 3, 1008);
INSERT INTO `sys_role_menu` VALUES (178, 3, 1009);
INSERT INTO `sys_role_menu` VALUES (179, 3, 1010);
INSERT INTO `sys_role_menu` VALUES (180, 3, 1011);
INSERT INTO `sys_role_menu` VALUES (181, 3, 1012);
INSERT INTO `sys_role_menu` VALUES (182, 3, 102);
INSERT INTO `sys_role_menu` VALUES (183, 3, 1013);
INSERT INTO `sys_role_menu` VALUES (184, 3, 1014);
INSERT INTO `sys_role_menu` VALUES (185, 3, 1015);
INSERT INTO `sys_role_menu` VALUES (186, 3, 1016);
INSERT INTO `sys_role_menu` VALUES (187, 3, 103);
INSERT INTO `sys_role_menu` VALUES (188, 3, 1017);
INSERT INTO `sys_role_menu` VALUES (189, 3, 1018);
INSERT INTO `sys_role_menu` VALUES (190, 3, 1019);
INSERT INTO `sys_role_menu` VALUES (191, 3, 1020);
INSERT INTO `sys_role_menu` VALUES (192, 3, 105);
INSERT INTO `sys_role_menu` VALUES (193, 3, 1026);
INSERT INTO `sys_role_menu` VALUES (194, 3, 1027);
INSERT INTO `sys_role_menu` VALUES (195, 3, 1028);
INSERT INTO `sys_role_menu` VALUES (196, 3, 1029);
INSERT INTO `sys_role_menu` VALUES (197, 3, 1030);
INSERT INTO `sys_role_menu` VALUES (198, 3, 107);
INSERT INTO `sys_role_menu` VALUES (199, 3, 1031);
INSERT INTO `sys_role_menu` VALUES (200, 3, 1032);
INSERT INTO `sys_role_menu` VALUES (201, 3, 1033);
INSERT INTO `sys_role_menu` VALUES (202, 3, 1034);
INSERT INTO `sys_role_menu` VALUES (203, 3, 1035);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '所属部门',
  `dept_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属部门名称',
  `post_ids` json NULL COMMENT '岗位组',
  `icon` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `nick_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别，0:男 1:女 2:未知',
  `note` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '启用状态，0:禁用 1:启用',
  `login_time` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '最后登陆IP',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `modify_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `valid` int NULL DEFAULT 1 COMMENT '有效状态，0:无效 1:有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$lbpXb6YzzqdnIw0naHGZkOS0qMMBMUjm7IXG6UYQUmlW65DQS.vTy', 1, 'Harry科技', '[]', 'http://localhost:9090/harry/20250110/2baf828d871e490a9ea1ecf19fc9a01b.png', '183865800@qq.com', '16666666666', 'Harry', '1', NULL, '1', '2024-11-20 17:25:53', '192.168.31.7', '2019-09-29 13:55:30', NULL, '2025-01-10 23:02:20', 'admin', 1);
INSERT INTO `sys_user` VALUES (101, 'harry', '$2a$10$Wr0wX8Ueiox9ndNXsrx8JOfe3sc6QIsTW4JW/rPR6.7iAzrdKLYSe', 2, '研发部', '[\"3\"]', '', '183865800@qq.com', '17777777777', 'Harry测试', '1', NULL, '1', '2024-11-20 17:32:24', '192.168.31.7', '2019-09-29 13:55:30', NULL, '2024-12-02 10:59:49', 'harry', 1);
INSERT INTO `sys_user` VALUES (102, 'chanpin_1', '$2a$10$06Yz1xjErlIpXFtgC8CLC.6zpqUp2ctC9ysW3qSl/Vof2qSZbbyGa', 6, '产品2部', NULL, NULL, '17777777777@163.com', '17777777777', '产品1部', '0', NULL, '1', NULL, NULL, '2024-12-27 15:11:05', NULL, '2024-12-31 09:07:02', NULL, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户和角色关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 101, 2);
INSERT INTO `sys_user_role` VALUES (2, 1, 1);
INSERT INTO `sys_user_role` VALUES (27, 102, 3);

SET FOREIGN_KEY_CHECKS = 1;
