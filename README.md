<h4 align="center">基于SpringBoot3+Vue3前后端分离的Java快速开发框架</h4>
<p align="center">
	<a href="https://gitee.com/harry-tech/harry/stargazers"><img
			src="https://gitee.com/harry-tech/harry/badge/star.svg?theme=gvp"></a>
	<a href="https://gitee.com/harry-tech/harry"><img
			src="https://img.shields.io/badge/harry-v2.0.0-brightgreen.svg"></a>
	<a href="https://gitee.com/harry-tech/harry/blob/master/LICENSE"><img
			src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>


## 平台简介
基于 JDK 17、Spring Boot 3、Spring Security 6、JWT、Redis、Mybatis-Plus、Knife4j等构建后端，基于Vue 3、Element-Plus 、TypeScript等构建前端的分离单体权限管理系统。

- **🚀 开发框架**: 使用 Spring Boot 3 和 Vue 3，以及 Element-Plus 等主流技术栈，实时更新。

- **🔐 安全认证**: 结合 Spring Security 和 JWT 提供安全、无状态、分布式友好的身份验证和授权机制。

- **🔑 权限管理**: 基于 RBAC 模型，实现细粒度的权限控制，涵盖接口方法和按钮级别。

- **🛠️ 功能模块**: 包括用户管理、角色管理、菜单管理、部门管理、字典管理等多个功能。

- **📘 接口文档**: 自动生成接口文档，支持在线调试，提高开发效率。



## 内置功能

- 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
- 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
- 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
- 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
- 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
- 参数管理：对系统动态配置常用参数。
- 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
- 登录日志：系统登录日志记录查询包含登录异常。
- 系统接口：根据业务代码自动生成相关的api接口文档，引入swagger接口文档服务的工具（Knife4j）。
<br> 

**技术选型：** 

| 依赖           | 版本            |
|--------------|---------------|
| Spring Boot  | 3.3.5 |
| Mybatis-Plus | 3.5.9        |
| hutool       | 5.8.26         |
| knife4j      | 4.5.0         |
| ...    | ...         |

<br> 


 ### 后端开发
 
**Gitee仓库地址:** https://gitee.com/harry-tech/harry.git

- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法
- 创建数据库harry3，数据库编码为UTF-8
- 执行db/harry3.sql文件，初始化数据
- 修改application.yml，更新MySQL账号和密码
- Eclipse、IDEA运行HarryApplication.java，则可启动项目
- Swagger注解路径：http://localhost:9999/doc.html
<br> 

### 前端开发
 - 本项目是前后端分离的，还需要部署前端，才能运行起来
 - 前端下载地址：
> - **Gitee:** https://gitee.com/harry-tech/harry-vue.git
 - 前端部署文档：-
 - 前端部署完毕，就可以访问项目了，账号：admin，密码：123456
 <br>

### 页面演示
![输入图片说明](db/img/Watermark_%E6%B0%B4%E5%8D%B0_20241231_120615_compressed.gif)

![输入图片说明](db/img/%E6%9A%97%E9%BB%91_20241231_121444_compressed.gif)