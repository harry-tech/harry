package cn.harry.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.*;

@SpringBootTest
class HarryApplicationTests {

    @Test
    void contextLoads() {
    }
    public static void main(String[] args) {

        // 初始化驱动
        try {
            // 驱动类com.mysql.jdbc.Driver
            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("数据库驱动加载成功 ！");

            //创建连接
            Connection c = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/db_626?allowPublicKeyRetrieval=true&useSSL=false", "root", "root");

            System.out.println("连接成功，获取连接对象： " + c);
            Statement s = c.createStatement();

            System.out.println("获取 Statement对象： " + s);

            String sql = "select * from tb_user;";
            //执行SQL语句
            ResultSet res = s.executeQuery(sql);
            while (res.next()) {
                System.out.println("用户名：" + res.getString(2) + "密码：" + res.getString(3));
            }

            res.close();
            s.close();
            c.close();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
