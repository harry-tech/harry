package ${packageName}.${moduleName}.${subpackageName};

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import ${packageName}.${moduleName}.service.${entityName}Service;
import ${packageName}.${moduleName}.domain.${entityName};
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * $!{businessName}前端控制层
 *
 * @author harry
 * @公众号 Harry技术
 */
@Tag(name = "${businessName}接口")
@RestController
@RequestMapping("/${lowerFirstEntityName}")
@RequiredArgsConstructor
public class ${entityName}Controller  {

    private final ${entityName}Service ${lowerFirstEntityName}Service;

    /**
     * 分页查询列表
     */
    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('${tableName}_page')")
    @GetMapping(value = "/page")
    public R<IPage<${entityName}>> page(@ParameterObject Page<${entityName}> page,@ParameterObject  ${entityName} entity) {
        return R.success(${lowerFirstEntityName}Service.page(page, Wrappers.lambdaQuery(entity)));
    }

    /**
     * 根据id获取详情
     */
    @Operation(summary = "根据id获取详情")
    @PreAuthorize("@ss.hasPermission('${tableName}_get')")
    @GetMapping(value = "/{id}")
    public R<${entityName}> getById(@PathVariable Long id) {
        return R.success(${lowerFirstEntityName}Service.getById(id));
    }

    /**
     * 新增
     */
    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('${tableName}_add')")
    @SysLog(title = "${tableName}", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody ${entityName} entity) {
        return ${lowerFirstEntityName}Service.save(entity) ? R.success() : R.failed();
    }

    /**
     * 更新
     */
    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('${tableName}_edit')")
    @SysLog(title = "${tableName}", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody ${entityName} entity) {
        return ${lowerFirstEntityName}Service.updateById(entity) ? R.success() : R.failed();
    }

    /**
     * 删除
     */
    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('${tableName}_del')")
    @SysLog(title = "${tableName}", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return ${lowerFirstEntityName}Service.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }
}
