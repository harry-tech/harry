package cn.harry.modular.oss.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * ClassName: OssCallbackResult Description: oss上传文件的回调结果
 *
 * @author honghh Date 2019/10/14 11:50 Copyright (C) www.tech-harry.cn
 */
@Data
public class OssCallbackResult {

	@Schema(description = "文件名称")
	private String filename;

	@Schema(description = "文件大小")
	private String size;

	@Schema(description = "文件的mimeType")
	private String mimeType;

	@Schema(description = "图片文件的宽")
	private String width;

	@Schema(description = "图片文件的高")
	private String height;

}
