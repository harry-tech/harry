package cn.harry.modular.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface OssService {

    /**
     * 上传文件
     * @param file 表单文件对象
     * @return 文件信息
     */
    String uploadFile(MultipartFile file);

    /**
     * 删除文件
     *
     * @param filePath 文件完整URL
     * @return 删除结果
     */
    boolean deleteFile(String filePath);
}
