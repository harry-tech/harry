package cn.harry.modular.oss.factory;

import cn.harry.common.utils.SpringUtils;
import cn.harry.modular.system.domain.SysFile;
import cn.harry.modular.system.domain.SysUser;
import cn.harry.modular.system.service.SysFileService;

import java.util.TimerTask;

/**
 * @author harry
 * @公众号 Harry技术
 */
public class OssAsyncFactory {

    public static TimerTask saveFileTask(final SysFile file, SysUser sysUser) {

        return new TimerTask() {
            @Override
            public void run() {
                if (sysUser != null) {
                    file.setCreateBy(sysUser.getUsername());
                    file.setModifyBy(sysUser.getUsername());
                }
                SpringUtils.getBean(SysFileService.class).save(file);
            }
        };
    }
}
