package cn.harry.modular.codegen.mapper;

import cn.harry.modular.codegen.model.entity.GenFieldConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 代码生成字段配置访问层
 *
 * @author Ray
 * @since 2.10.0
 */
@Mapper
public interface GenFieldConfigMapper extends BaseMapper<GenFieldConfig> {

}




