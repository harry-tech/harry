package cn.harry.modular.codegen.controller;

import cn.harry.common.api.R;
import cn.harry.config.property.CodegenProperties;
import cn.harry.modular.codegen.model.form.GenConfigForm;
import cn.harry.modular.codegen.model.query.TablePageQuery;
import cn.harry.modular.codegen.model.vo.CodegenPreviewVO;
import cn.harry.modular.codegen.model.vo.TablePageVO;
import cn.harry.modular.codegen.service.CodegenService;
import cn.harry.modular.codegen.service.GenConfigService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 代码生成器控制层
 *
 * @author Ray
 * @since 2.10.0
 */
@Tag(name = "代码生成")
@RestController
@RequestMapping("/codegen")
@RequiredArgsConstructor
@Slf4j
public class CodegenController {

    private final CodegenService codegenService;
    private final GenConfigService genConfigService;
    private final CodegenProperties codegenProperties;

    @Operation(summary = "获取数据表分页列表")
    @GetMapping("/table/page")
    public R<Page<TablePageVO>> getTablePage(TablePageQuery queryParams) {
        Page<TablePageVO> result = codegenService.getTablePage(queryParams);
        return R.success(result);
    }

    @Operation(summary = "获取代码生成配置")
    @GetMapping("/{tableName}/config")
    public R<GenConfigForm> getGenConfigFormData(@Parameter(description = "表名", example = "sys_user") @PathVariable String tableName) {
        GenConfigForm formData = genConfigService.getGenConfigFormData(tableName);
        return R.success(formData);
    }

    @Operation(summary = "保存代码生成配置")
    @PostMapping("/{tableName}/config")
    public R<?> saveGenConfig(@RequestBody GenConfigForm formData) {
        genConfigService.saveGenConfig(formData);
        return R.success();
    }

    @Operation(summary = "删除代码生成配置")
    @DeleteMapping("/{tableName}/config")
    public R<?> deleteGenConfig(@Parameter(description = "表名", example = "sys_user") @PathVariable String tableName) {
        genConfigService.deleteGenConfig(tableName);
        return R.success();
    }

    @Operation(summary = "获取预览生成代码")
    @GetMapping("/{tableName}/preview")
    public R<List<CodegenPreviewVO>> getTablePreviewData(@PathVariable String tableName) {
        List<CodegenPreviewVO> list = codegenService.getCodegenPreviewData(tableName);
        return R.success(list);
    }

    @Operation(summary = "下载代码")
    @GetMapping("/{tableName}/download")
    public void downloadZip(HttpServletResponse response, @PathVariable String tableName) {
        String[] tableNames = tableName.split(",");
        byte[] data = codegenService.downloadCode(tableNames);
        String downloadFileName = codegenProperties.getDownloadFileName() + "_" + System.currentTimeMillis()+ ".zip";
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(downloadFileName, StandardCharsets.UTF_8));
        response.setContentType("application/octet-stream; charset=UTF-8");

        try (ServletOutputStream outputStream = response.getOutputStream()) {
            outputStream.write(data);
            outputStream.flush();
        } catch (IOException e) {
            log.error("Error while writing the zip file to response", e);
            throw new RuntimeException("Failed to write the zip file to response", e);
        }
    }
}
