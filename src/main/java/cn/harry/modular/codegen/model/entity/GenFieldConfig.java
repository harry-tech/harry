package cn.harry.modular.codegen.model.entity;

import cn.harry.modular.codegen.enums.FormTypeEnum;
import cn.harry.modular.codegen.enums.QueryTypeEnum;
import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * 字段生成配置实体
 *
 * @author Ray
 * @since 2.10.0
 */
@TableName(value = "gen_field_config")
@Data
public class GenFieldConfig {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    @Schema(description = "ID")
    private Long id;
    /**
     * 关联的配置ID
     */
    private Long configId;

    /**
     * 列名
     */
    private String columnName;

    /**
     * 列类型
     */
    private String columnType;

    /**
     * 字段长度
     */
    private Long maxLength;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段排序
     */
    private Integer fieldSort;

    /**
     * 字段类型
     */
    private String fieldType;

    /**
     * 字段描述
     */
    private String fieldComment;

    /**
     * 表单类型
     */
    private FormTypeEnum formType;

    /**
     * 查询方式
     */
    private QueryTypeEnum queryType;

    /**
     * 是否在列表显示
     */
    private Integer isShowInList;

    /**
     * 是否在表单显示
     */
    private Integer isShowInForm;

    /**
     * 是否在查询条件显示
     */
    private Integer isShowInQuery;

    /**
     * 是否必填
     */
    private Integer isRequired;

    /**
     * TypeScript类型
     */
    @TableField(exist = false)
    @JsonIgnore
    private String tsType;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date modifyTime;


}
