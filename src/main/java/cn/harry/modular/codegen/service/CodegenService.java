package cn.harry.modular.codegen.service;


import cn.harry.modular.codegen.model.query.TablePageQuery;
import cn.harry.modular.codegen.model.vo.CodegenPreviewVO;
import cn.harry.modular.codegen.model.vo.TablePageVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * 代码生成配置接口
 *
 * @author Ray
 * @since 2.10.0
 */
public interface CodegenService {

    /**
     * 获取数据表分页列表
     *
     * @param queryParams 查询参数
     * @return
     */
    Page<TablePageVO> getTablePage(TablePageQuery queryParams);

    /**
     * 获取预览生成代码
     *
     * @param tableName 表名
     * @return
     */
    List<CodegenPreviewVO> getCodegenPreviewData(String tableName);

    /**
     * 下载代码
     * @param tableNames 表名
     * @return
     */
    byte[] downloadCode(String[] tableNames);
}
