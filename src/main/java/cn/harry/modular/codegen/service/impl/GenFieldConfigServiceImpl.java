package cn.harry.modular.codegen.service.impl;

import cn.harry.modular.codegen.mapper.GenFieldConfigMapper;
import cn.harry.modular.codegen.model.entity.GenFieldConfig;
import cn.harry.modular.codegen.service.GenFieldConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 代码生成字段配置服务实现类
 *
 * @author Ray
 * @since 2.10.0
 */
@Service
@RequiredArgsConstructor
public class GenFieldConfigServiceImpl extends ServiceImpl<GenFieldConfigMapper, GenFieldConfig> implements GenFieldConfigService {


}
