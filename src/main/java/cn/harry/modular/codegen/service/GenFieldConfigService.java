package cn.harry.modular.codegen.service;


import cn.harry.modular.codegen.model.entity.GenFieldConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 代码生成配置接口
 *
 * @author Ray
 * @since 2.10.0
 */
public interface GenFieldConfigService extends IService<GenFieldConfig> {

}
