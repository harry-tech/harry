package cn.harry.modular.system.mapper;

import cn.harry.common.annotation.DataScope;
import cn.harry.modular.system.domain.SysUser;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {


    @Override
    @DataScope(userIdColumnName = "id")
    List<SysUser> selectList(IPage<SysUser> page, @Param(Constants.WRAPPER) Wrapper<SysUser> queryWrapper);

    /**
     * 根据用户名查询用户
     *
     * @param username
     * @return
     */
    SysUser selectByUsername(String username);
}




