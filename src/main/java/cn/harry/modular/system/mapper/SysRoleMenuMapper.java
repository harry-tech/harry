package cn.harry.modular.system.mapper;

import cn.harry.modular.system.domain.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author harry
* @公众号 Harry技术
*/
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {


    List<Long> listMenuIdsByRoleId(Long roleId);
}




