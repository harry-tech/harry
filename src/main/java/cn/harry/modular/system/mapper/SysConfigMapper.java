package cn.harry.modular.system.mapper;

import cn.harry.modular.system.domain.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author harry
* @公众号 Harry技术
*/
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}




