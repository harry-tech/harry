package cn.harry.modular.system.mapper;

import cn.harry.modular.system.domain.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
* @author harry
* @公众号 Harry技术
*/
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    /**
     * 获取用户角色标识
     *
     * @param userId
     * @return
     */
    Set<String> listRoleKeyByUserId(Long userId);
    /**
     * 获取最大的数据权限
     *
     * @param roles 角色标识
     * @return 数据权限 value
     */
    Integer getMaximumDataScope(Set<String> roles);
    /**
     * 根据用户id 批量添加用户角色关系
     *
     * @param userId
     * @param roleIdList
     */
    void insertUserAndUserRole(@Param("userId") Long userId, @Param("roleIdList") List<Long> roleIdList);

}




