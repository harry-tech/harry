package cn.harry.modular.system.mapper;

import cn.harry.modular.system.domain.SysFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件管理Mapper接口
 *
 * @author harry
 * @公众号 Harry技术
 */
@Mapper
public interface SysFileMapper extends BaseMapper<SysFile> {

}
