package cn.harry.modular.system.mapper;

import cn.harry.modular.system.domain.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
* @author harry
* @公众号 Harry技术
*/
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    Set<String> getMenuPermission(@Param("userId") Long useId);

    /**
     * 根据用户ID查询菜单
     * @param userId
     * @return
     */
    List<SysMenu> getRoutersByUserId(@Param("userId") Long userId);

    /**
     * 获取用户所有权限
     * @param userId
     * @return
     */
    List<SysMenu> getPermissionList(@Param("userId") Long userId);
}




