package cn.harry.modular.system.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Builder
@Data
public class CaptchaResult {
    /**
     * 验证码缓存key
     */
    @Schema(description = "验证码缓存key")
    private String uuid;
    /**
     * 验证码图片Base64字符串
     */
    @Schema(description = "验证码图片Base64字符串")
    private String img;
    /**
     * +
     * 验证码是否开启
     */
    @Schema(description = "验证码是否开启")
    private boolean enabled;
}
