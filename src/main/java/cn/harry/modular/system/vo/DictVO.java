package cn.harry.modular.system.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * @author harry
 * @公众号 Harry技术
 */
@Schema(description = "字典数据分页对象")
@Getter
@Setter
public class DictVO {


    /**
     * 字典名称
     */
    @Schema(description = "字典名称")
    private String name;

    /**
     * 字典类型
     */
    @Schema(description = "字典类型")
    private String type;

    @Schema(description = "字典数据集合")
    private List<DictData> data;

    @Schema(description = "字典数据")
    @Getter
    @Setter
    public static class DictData {

        @Schema(description = "字典数据值")
        private String value;

        @Schema(description = "字典数据标签")
        private String label;

        @Schema(description = "标签类型")
        private String tagType;
    }

}
