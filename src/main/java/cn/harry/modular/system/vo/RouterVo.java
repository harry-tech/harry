package cn.harry.modular.system.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 菜单路由
 *
 * @author harry
 * @公众号 Harry技术
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RouterVo {

    /**
     * 路由名字
     */
    @Schema(description = "路由名字")
    private String name;

    /**
     * 路由地址
     */
    @Schema(description = "路由地址", example = "user")
    private String path;

    /**
     * 是否隐藏路由，当设置 true 的时候该路由不会再侧边栏出现
     */
    @Schema(description = "是否隐藏路由，当设置 true 的时候该路由不会再侧边栏出现")
    private String hidden;

    /**
     * 重定向地址，当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
     */
    @Schema(description = "重定向地址，当设置 noRedirect 的时候该路由在面包屑导航中不可被点击")
    private String redirect;

    /**
     * 组件地址
     */
    @Schema(description = "组件地址", example = "system/user/index")
    private String component;

    /**
     * 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
     */
    @Schema(description = "当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面")
    private Boolean alwaysShow;

    /**
     * 其他元素
     */
    @Schema(description = "路由属性")
    private MetaVo meta;

    @Data
    @Schema(description = "路由属性类型")
    public static class MetaVo {

        /**
         * 设置该路由在侧边栏和面包屑中展示的名字
         */
        @Schema(description = "设置该路由在侧边栏和面包屑中展示的名字")
        private String title;

        /**
         * icon
         */
        @Schema(description = "icon")
        private String icon;


        @Schema(description = "是否隐藏(true-是 false-否)", example = "true")
        private Boolean hidden;

        @Schema(description = "【菜单】是否开启页面缓存", example = "true")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Boolean keepAlive;

        @Schema(description = "【目录】只有一个子路由是否始终显示", example = "true")
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Boolean alwaysShow;

        @Schema(description = "路由参数")
        private Map<String,Object> params;

    }

    /**
     * 子路由
     */
    @Schema(description = "子路由")
    private List<RouterVo> children;

}
