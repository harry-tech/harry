package cn.harry.modular.system.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Map;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Data
@Schema(description = "MetaVo")
public class MetaVo {

    /**
     * 设置该路由在侧边栏和面包屑中展示的名字
     */
    @Schema(description = "设置该路由在侧边栏和面包屑中展示的名字")
    private String title;

    /**
     * 设置该路由的图标，对应路径src/icons/svg
     */
    @Schema(description = "设置该路由的图标，对应路径src/icons/svg")
    private String icon;


    @Schema(description = "是否隐藏(true-是 false-否)", example = "true")
    private Boolean hidden;

    @Schema(description = "【菜单】是否开启页面缓存", example = "true")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean keepAlive;

    @Schema(description = "【目录】只有一个子路由是否始终显示", example = "true")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean alwaysShow;

    @Schema(description = "路由参数")
    private Map<String,String> params;

    public MetaVo() {
    }

    public MetaVo(String title, String icon) {
        this.title = title;
        this.icon = icon;
    }

}
