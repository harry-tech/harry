package cn.harry.modular.system.enums;

/**
 * @author harry
 * @公众号 Harry技术
 */
public enum BucketTypeEnums {

    /**
     * 阿里云存储
     */
    ALIYUN,
    /**
     * MINIO 存储
     */
    MINIO
}
