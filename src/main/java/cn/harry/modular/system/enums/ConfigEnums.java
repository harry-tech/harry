package cn.harry.modular.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author harry
 */
@Getter
@AllArgsConstructor
public enum ConfigEnums {

    SYS_CAPTCHA_IMG("图片验证码开关"),

    ;

    private final String desc;

}
