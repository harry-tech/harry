package cn.harry.modular.system.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 菜单类型
 *
 * @author harry
 */
@Getter
@AllArgsConstructor
public enum MenuTypeEnums {

    /**
     * 目录
     */
    CATALOG(0),
    /**
     * 菜单
     */
    MENU(1),
    /**
     * 按钮
     */
    BUTTON(2),
    /**
     * 外链
     */
    EX_LINK(3);

    @EnumValue
    private final Integer value;

}
