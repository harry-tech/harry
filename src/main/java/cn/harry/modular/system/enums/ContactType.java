package cn.harry.modular.system.enums;

/**
 * @author harry
 * @公众号 Harry技术
 */
public enum ContactType {
    /**
     * 手机
     */
    MOBILE,

    /**
     * 邮箱
     */
    EMAIL
}
