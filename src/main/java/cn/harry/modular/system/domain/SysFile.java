package cn.harry.modular.system.domain;

import cn.harry.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 文件管理实体对象
 *
 * @author harry
 * @公众号 Harry技术
 */
@Getter
@Setter
@TableName("sys_file")
public class SysFile extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 文件名
     */
    private String fileName;
    /**
     * 原始文件名
     */
    private String original;
    /**
     * 文件存储桶名称
     */
    private String bucketName;
    /**
     * 桶类型 aliyun,minio
     */
    private String bucketType;
    /**
     * 文件类型
     */
    private String type;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 访问地址
     */
    private String domain;
    /**
     * 有效状态，0:无效 1:有效
     */
    private Integer valid;

    public SysFile() {
    }

    public SysFile(String bucketType,String fileName, String original, String bucketName,  String type, Long fileSize, String domain) {
        this.bucketType = bucketType;
        this.fileName = fileName;
        this.original = original;
        this.bucketName = bucketName;
        this.type = type;
        this.fileSize = fileSize;
        this.domain = domain;
    }
}
