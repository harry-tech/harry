package cn.harry.modular.system.domain;

import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 角色表
 *
 * @author harry
 * @公众号 Harry技术
 */
@TableName(value = "sys_role")
@Data
public class SysRole implements Serializable {
    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String name;

    /**
     * 角色权限字符
     */
    @Schema(description = "角色权限字符")
    private String roleKey;

    /**
     * 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 ）
     */
    @Schema(description = "数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 ）")
    private String dataScope;

    /**
     * 描述
     */
    @Schema(description = "描述")
    private String description;

    /**
     * 后台用户数量
     */
    @Schema(description = "后台用户数量")
    private Integer userCount;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date createTime;

    /**
     * 启用状态，0:禁用 1:启用
     */
    @Schema(description = "启用状态，0:禁用 1:启用")
    private String status;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 有效状态，0:无效 1:有效
     */
    @Schema(description = "有效状态，0:无效 1:有效")
    @TableLogic
    private Integer valid;

    @Serial
    private static final long serialVersionUID = 1L;


}