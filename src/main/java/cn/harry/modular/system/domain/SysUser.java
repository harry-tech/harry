package cn.harry.modular.system.domain;

import cn.hutool.core.date.DatePattern;
import cn.hutool.json.JSONArray;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户表
 * @author harry
 * @公众号 Harry技术
 */
@TableName(value ="sys_user", autoResultMap = true)
@Data
public class SysUser implements Serializable {
    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    @TableId( type = IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    @Schema(description = "用户名")
    private String username;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 所属部门
     */
    @Schema(description = "所属部门")
    private Long deptId;

    /**
     * 所属部门名称
     */
    @Schema(description = "所属部门名称")
    private String deptName;

    /**
     * 岗位组
     */
    @Schema(description = "岗位组")
    @TableField(value = "post_ids", typeHandler = JacksonTypeHandler.class)
    private JSONArray postIds;

    /**
     * 头像
     */
    @Schema(description = "头像")
    private String icon;

    /**
     * 邮箱
     */
    @Schema(description = "邮箱")
    private String email;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 昵称
     */
    @Schema(description = "昵称")
    private String nickName;

    /**
     * 性别，0:男 1:女 2:未知
     */
    @Schema(description = "性别，0:男 1:女 2:未知")
    private String sex;

    /**
     * 备注信息
     */
    @Schema(description = "备注信息")
    private String note;

    /**
     * 启用状态，0:禁用 1:启用
     */
    @Schema(description = "启用状态，0:禁用 1:启用")
    private String status;

    /**
     * 最后登录时间
     */
    @Schema(description = "最后登录时间")
    private Date loginTime;

    /**
     * 最后登陆IP
     */
    @Schema(description = "最后登陆IP")
    private String loginIp;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date createTime;

    /**
     * 创建人
     */
    @Schema(description = "创建人")
    private String createBy;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date modifyTime;

    /**
     * 修改人
     */
    @Schema(description = "修改人")
    private String modifyBy;

    /**
     * 有效状态，0:无效 1:有效
     */
    @Schema(description = "有效状态，0:无效 1:有效")
    @TableLogic
    private Integer valid;

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 角色ID列表
     */
    @Schema(description = "角色ID列表")
    @TableField(exist = false)
    private List<Long> roleIds;
}