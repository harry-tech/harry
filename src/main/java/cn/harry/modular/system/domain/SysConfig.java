package cn.harry.modular.system.domain;

import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统配置表
 *
 * @author harry
 * @公众号 Harry技术
 */
@TableName(value = "sys_config")
@Data
public class SysConfig implements Serializable {
    /**
     * 配置ID
     */
    @TableId(type = IdType.AUTO)
    @Schema(description = "配置ID")
    private Long id;

    /**
     * 系统内置: Y N
     */
    @Schema(description = "系统内置: Y N")
    private String configType;

    /**
     * 键
     */
    @Schema(description = "键")
    private String configKey;

    /**
     * 值
     */
    @Schema(description = "值")
    private String configValue;

    /**
     * 名称
     */
    @Schema(description = "名称")
    private String configName;

    /**
     * 状态， 0：禁用 1：启用
     */
    @Schema(description = "状态， 0：禁用 1：启用")
    private String status;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date modifyTime;

    /**
     * 有效状态，0:无效 1:有效
     */
    @Schema(description = "有效状态，0:无效 1:有效")
    @TableLogic
    private Integer valid;

    @Serial
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}