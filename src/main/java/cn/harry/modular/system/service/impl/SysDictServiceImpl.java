package cn.harry.modular.system.service.impl;

import cn.harry.modular.system.domain.SysDictData;
import cn.harry.modular.system.service.SysDictDataService;
import cn.harry.modular.system.vo.DictVO;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.modular.system.domain.SysDict;
import cn.harry.modular.system.service.SysDictService;
import cn.harry.modular.system.mapper.SysDictMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Service
@RequiredArgsConstructor
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict>
        implements SysDictService {

    private final SysDictDataService sysDictDataService;

    @Override
    public List<DictVO> getAllDictWithData() {
        List<SysDict> dicts = this.list();
        List<SysDictData> dictDatas = sysDictDataService.list();
        if (dicts != null && !dicts.isEmpty()) {
            return dicts.stream().map(dict -> {
                DictVO dictVO = new DictVO();
                dictVO.setName(dict.getName());
                dictVO.setType(dict.getType());
                dictVO.setData(covertData(dict.getType(), dictDatas));
                return dictVO;
            }).toList();
        }
        return null;
    }

    private List<DictVO.DictData> covertData(String type, List<SysDictData> dictDatas) {
        if (dictDatas != null && !dictDatas.isEmpty()) {
            return dictDatas.stream().filter(dictData -> type.equals(dictData.getDictType())).map(dictData -> {
                DictVO.DictData data = new DictVO.DictData();
                data.setLabel(dictData.getDictLabel());
                data.setValue(dictData.getDictValue());
                data.setTagType(dictData.getListClass());
                return data;
            }).toList();
        }
        return null;
    }

}




