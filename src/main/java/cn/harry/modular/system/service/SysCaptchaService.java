package cn.harry.modular.system.service;


import cn.harry.modular.system.vo.CaptchaResult;

/**
 * 图片验证码
 *
 * @author harry
 * @公众号 Harry技术
 */
public interface SysCaptchaService {

    /**
     * 验证码效验
     *
     * @param uuid uuid
     * @param code 验证码
     * @return true：成功 false：失败
     */
    boolean validate(String uuid, String code);

    /**
     * 获取验证码
     *
     * @return 验证码
     */
    CaptchaResult getCaptcha();
}
