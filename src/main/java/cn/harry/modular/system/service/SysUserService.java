package cn.harry.modular.system.service;

import cn.harry.modular.auth.vo.LoginResult;
import cn.harry.modular.auth.vo.UserInfoResult;
import cn.harry.modular.system.domain.SysUser;
import cn.harry.modular.system.enums.ContactType;
import cn.harry.modular.system.param.EmailBindingForm;
import cn.harry.modular.system.param.PhoneBindingForm;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * login
     *
     * @param username 用户名
     * @param password 密码
     * @return
     */
    LoginResult login(String username, String password);

    /**
     * get user info
     *
     * @return
     */
    UserInfoResult getInfo();

    /**
     * get page
     *
     * @param page
     * @param sysUser
     * @return
     */
    IPage<SysUser> getPage(Page<SysUser> page, SysUser sysUser);


    /**
     * 根据用户ID获取用户信息
     *
     * @param id
     * @return
     */
    SysUser getUserById(Long id);

    /**
     * 批量删除用户
     *
     * @param ids
     * @return
     */
    boolean deleteByIds(Long[] ids);

    /**
     * 更新用户
     *
     * @param user
     * @return
     */
    boolean updateUser(SysUser user);

    /**
     * 创建用户
     *
     * @param user
     * @return
     */
    boolean createUser(SysUser user);

    /**
     * 更新用户密码
     *
     * @param userId   用户ID
     * @param password 密码
     * @return
     */
    boolean updatePasswordByUserId(Long userId, String password);

    /**
     * 绑定邮箱
     *
     * @param param
     * @return
     */
    boolean bindEmail(EmailBindingForm param);

    /**
     * 绑定手机号
     *
     * @param param
     * @return
     */
    boolean bindPhone(PhoneBindingForm param);

    /**
     * 发送验证码
     *
     * @param contact
     * @param contactType
     * @return
     */
    boolean sendVerificationCode(String contact, ContactType contactType);
}
