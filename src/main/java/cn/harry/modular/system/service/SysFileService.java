package cn.harry.modular.system.service;

import cn.harry.modular.system.domain.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 文件管理服务类
 *
 * @author harry
 * @公众号 Harry技术
 */
public interface SysFileService extends IService<SysFile> {

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    boolean deleteBatchByIds(List<Long> ids);
}
