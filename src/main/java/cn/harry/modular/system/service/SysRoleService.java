package cn.harry.modular.system.service;

import cn.harry.common.model.Option;
import cn.harry.modular.system.domain.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 获取角色的菜单ID集合
     *
     * @param roleId 角色ID
     * @return 菜单ID集合(包括按钮权限ID)
     */
    List<Long> getRoleMenuIds(Long roleId);

    /**
     * 分配菜单权限
     *
     * @param roleId  角色ID
     * @param menuIds 菜单ID集合
     * @return 是否分配成功
     */
    boolean assignMenusToRole(Long roleId, List<Long> menuIds);

    /**
     * 角色下拉列表
     *
     * @return
     */
    List<Option<Long>> listRoleOptions();
}
