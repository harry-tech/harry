package cn.harry.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.modular.system.domain.SysDictData;
import cn.harry.modular.system.service.SysDictDataService;
import cn.harry.modular.system.mapper.SysDictDataMapper;
import org.springframework.stereotype.Service;

/**
* @author harry
* @公众号 Harry技术
*/
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData>
    implements SysDictDataService{

}




