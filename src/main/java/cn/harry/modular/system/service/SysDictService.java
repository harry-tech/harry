package cn.harry.modular.system.service;

import cn.harry.modular.system.domain.SysDict;
import cn.harry.modular.system.vo.DictVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface SysDictService extends IService<SysDict> {

    /**
     * 获取所有的字典以及字典数据
     *
     * @return
     */
    List<DictVO> getAllDictWithData();
}
