package cn.harry.modular.system.service;

import cn.harry.common.model.Option;
import cn.harry.modular.system.domain.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface SysDeptService extends IService<SysDept> {
    /**
     * 查询部门管理数据
     *
     * @param sysDept 部门信息
     * @return 部门
     */
    List<SysDept> selectDeptList(SysDept sysDept);

    /**
     * 查询部门管理树
     *
     * @return
     */
    List<Option<Long>> listDeptOptions();

    
    boolean createDept(SysDept sysDept);

    boolean updateDept(SysDept sysDept);
}
