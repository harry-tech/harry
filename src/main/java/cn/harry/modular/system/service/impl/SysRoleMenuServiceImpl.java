package cn.harry.modular.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.modular.system.domain.SysRoleMenu;
import cn.harry.modular.system.service.SysRoleMenuService;
import cn.harry.modular.system.mapper.SysRoleMenuMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {
    @Override
    public List<Long> listMenuIdsByRoleId(Long roleId) {
        return this.baseMapper.listMenuIdsByRoleId(roleId);
    }

    @Override
    public void refreshRolePermsCache(String roleKey) {

    }
}




