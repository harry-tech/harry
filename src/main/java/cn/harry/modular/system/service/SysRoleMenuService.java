package cn.harry.modular.system.service;

import cn.harry.modular.system.domain.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author harry
* @公众号 Harry技术
*/
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 获取角色拥有的菜单ID集合
     *
     * @param roleId 角色ID
     * @return 菜单ID集合
     */
    List<Long> listMenuIdsByRoleId(Long roleId);

    /**
     *  刷新权限缓存
     * @param roleKey
     */
    void refreshRolePermsCache(String roleKey);
}
