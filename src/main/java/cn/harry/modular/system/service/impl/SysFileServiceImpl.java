package cn.harry.modular.system.service.impl;

import cn.harry.modular.oss.service.OssService;
import cn.harry.modular.system.domain.SysFile;
import cn.harry.modular.system.mapper.SysFileMapper;
import cn.harry.modular.system.service.SysFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件管理服务实现类
 *
 * @author harry
 * @公众号 Harry技术
 */
@Service
@RequiredArgsConstructor
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements SysFileService {
    private final OssService ossService;

    @Override
    public boolean deleteBatchByIds(List<Long> ids) {
        for (Long id : ids) {
            ossService.deleteFile(getById(id).getDomain());
            removeById(id);
        }
        return true;
    }
}
