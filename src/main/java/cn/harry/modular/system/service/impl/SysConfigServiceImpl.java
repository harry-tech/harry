package cn.harry.modular.system.service.impl;

import cn.harry.common.constant.CacheConstants;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.harry.modular.system.domain.SysConfig;
import cn.harry.modular.system.service.SysConfigService;
import cn.harry.modular.system.mapper.SysConfigMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig>
        implements SysConfigService {

    @Override
    @Cacheable(value = CacheConstants.CONFIG_DETAILS, key = "#key", unless = "#result == null ")
    public String getKeyToValue(String key) {
        SysConfig sysConfig = this.getOne(new LambdaQueryWrapper<SysConfig>().eq(SysConfig::getConfigKey, key));
        if (sysConfig != null) {
            return sysConfig.getConfigKey();
        }
        return null;
    }
}




