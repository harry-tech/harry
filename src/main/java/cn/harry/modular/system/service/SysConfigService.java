package cn.harry.modular.system.service;

import cn.harry.modular.system.domain.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author harry
* @公众号 Harry技术
*/
public interface SysConfigService extends IService<SysConfig> {

    /**
     * 根据键名查询参数配置信息
     * @param key 参数键名
     * @return 参数键值
     */
    String getKeyToValue(String key);
}
