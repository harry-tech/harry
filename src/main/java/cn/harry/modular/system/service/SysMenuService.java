package cn.harry.modular.system.service;

import cn.harry.common.model.Option;
import cn.harry.modular.system.domain.SysMenu;
import cn.harry.modular.system.vo.RouterVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 根据用户ID查询菜单树信息
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    List<SysMenu> getRouters(Long userId);

    /**
     * 构建前端路由所需要的菜单
     *
     * @param menus 菜单列表
     * @return 路由列表
     */
    List<RouterVo> buildMenus(List<SysMenu> menus);

    /**
     * 根据用户ID查询菜单列表
     *
     * @param menu
     * @param userId
     * @return
     */
    List<SysMenu> selectMenuList(SysMenu menu, Long userId);
    /**
     * 获取菜单下拉列表
     *
     * @param onlyParent 是否只查询父级菜单
     */
    List<Option> listMenuOptions(boolean onlyParent);

    /**
     * 更新或保存菜单
     * @param sysMenu
     * @return
     */
    boolean saveMenu(SysMenu sysMenu);
}
