package cn.harry.modular.system.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 修改头像
 *
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class SysUserUpdateIconParam {

    @Schema(description = "修改头像")
    private String icon;

}
