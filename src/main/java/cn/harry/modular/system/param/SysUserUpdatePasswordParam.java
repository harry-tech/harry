package cn.harry.modular.system.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 修改密码参数
 *
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class SysUserUpdatePasswordParam {

    @Schema(description = "密码")
    private String oldPassword;

    @Schema(description = "新密码")
    private String newPassword;

}
