package cn.harry.modular.system.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class EmailBindingForm {

    @Schema(description = "邮箱")
    private String email;

    @Schema(description = "验证码")
    private String code;
}
