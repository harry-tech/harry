package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.modular.system.service.SysFileService;
import cn.harry.modular.system.domain.SysFile;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 文件管理前端控制层
 *
 * @author harry
 * @公众号 Harry技术
 */
@Tag(name = "文件管理接口")
@RestController
@RequestMapping("/sys/file")
@RequiredArgsConstructor
public class SysFileController  {

    private final SysFileService sysFileService;

    /**
     * 分页查询列表
     */
    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_file_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysFile>> page(@ParameterObject Page<SysFile> page,@ParameterObject  SysFile entity) {
        return R.success(sysFileService.page(page, Wrappers.lambdaQuery(entity)));
    }

    /**
     * 根据id获取详情
     */
    @Operation(summary = "根据id获取详情")
    @PreAuthorize("@ss.hasPermission('sys_file_get')")
    @GetMapping(value = "/{id}")
    public R<SysFile> getById(@PathVariable Long id) {
        return R.success(sysFileService.getById(id));
    }

    /**
     * 新增
     */
    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_file_add')")
    @SysLog(title = "sys_file", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysFile entity) {
        return sysFileService.save(entity) ? R.success() : R.failed();
    }

    /**
     * 更新
     */
    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_file_edit')")
    @SysLog(title = "sys_file", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysFile entity) {
        return sysFileService.updateById(entity) ? R.success() : R.failed();
    }

    /**
     * 删除
     */
    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_file_del')")
    @SysLog(title = "sys_file", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysFileService.deleteBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }
}
