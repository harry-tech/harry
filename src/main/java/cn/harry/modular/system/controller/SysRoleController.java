package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.common.model.Option;
import cn.harry.modular.system.domain.SysRole;
import cn.harry.modular.system.service.SysRoleService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 角色管理
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "角色管理")
@RequestMapping("/sys/role")
public class SysRoleController {
    private final SysRoleService sysRoleService;


    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_role_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysRole>> page(@ParameterObject Page<SysRole> page, @ParameterObject SysRole sysRole) {
        return R.success(sysRoleService.page(page, Wrappers.lambdaQuery(sysRole)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_role_get')")
    @GetMapping(value = "/{id}")
    public R<SysRole> getById(@PathVariable Long id) {
        return R.success(sysRoleService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_role_add')")
    @SysLog(title = "sys_role", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysRole sysRole) {
        return sysRoleService.save(sysRole) ? R.success() : R.failed();
    }


    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_role_edit')")
    @SysLog(title = "sys_role", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysRole sysRole) {
        return sysRoleService.updateById(sysRole) ? R.success() : R.failed();
    }

    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_role_del')")
    @SysLog(title = "sys_role", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysRoleService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }

    @Operation(summary = "获取角色的菜单ID集合")
    @GetMapping("/{roleId}/menuIds")
    public R<List<Long>> getRoleMenuIds(@Parameter(description = "角色ID") @PathVariable Long roleId) {
        List<Long> menuIds = sysRoleService.getRoleMenuIds(roleId);
        return R.success(menuIds);
    }

    @Operation(summary = "分配菜单(包括按钮权限)给角色")
    @PutMapping("/{roleId}/menus")
    public R<?> assignMenusToRole(@PathVariable Long roleId, @RequestBody List<Long> menuIds) {
        boolean result = sysRoleService.assignMenusToRole(roleId, menuIds);
        return R.success(result);
    }

    @Operation(summary = "角色下拉列表")
    @GetMapping("/options")
    public R<List<Option<Long>>> listRoleOptions() {
        List<Option<Long>> list = sysRoleService.listRoleOptions();
        return R.success(list);
    }
}
