package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.common.model.Option;
import cn.harry.modular.system.domain.SysDept;
import cn.harry.modular.system.service.SysDeptService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 部门管理
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "部门管理")
@RequestMapping("/sys/dept")
public class SysDeptController {
    private final SysDeptService sysDeptService;


    @Operation(summary = "部门下拉列表")
    @GetMapping("/options")
    public R<List<Option<Long>>> getDeptOptions() {
        List<Option<Long>> list = sysDeptService.listDeptOptions();
        return R.success(list);
    }


    @Operation(summary = "根据关键字获取部门列表")
    @PreAuthorize("@ss.hasPermission('system_dept_list')")
    @GetMapping(value = "/list")
    public R<List<SysDept>> list(SysDept sysDept) {
        List<SysDept> list = sysDeptService.selectDeptList(sysDept);
        return R.success(list);
    }

    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_dept_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysDept>> page(@ParameterObject Page<SysDept> page, @ParameterObject SysDept sysDept) {
        return R.success(sysDeptService.page(page, Wrappers.lambdaQuery(sysDept)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_dept_get')")
    @GetMapping(value = "/{id}")
    public R<SysDept> getById(@PathVariable Long id) {
        return R.success(sysDeptService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_dept_add')")
    @SysLog(title = "sys_dept", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysDept sysDept) {
        return sysDeptService.createDept(sysDept) ? R.success() : R.failed();
    }


    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_dept_edit')")
    @SysLog(title = "sys_dept", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysDept sysDept) {
        return sysDeptService.updateDept(sysDept) ? R.success() : R.failed();
    }


    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_dept_del')")
    @SysLog(title = "sys_dept", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysDeptService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }

}
