package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.common.model.Option;
import cn.harry.component.security.utils.SecurityUtils;
import cn.harry.modular.system.domain.SysMenu;
import cn.harry.modular.system.service.SysMenuService;
import cn.harry.modular.system.vo.RouterVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 菜单管理
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "菜单管理")
@RequestMapping("/sys/menu")
public class SysMenuController {
    private final SysMenuService sysMenuService;

    @Operation(summary = "菜单下拉列表")
    @GetMapping("/options")
    public R<?> listMenuOptions(@Parameter(description = "是否只查询父级菜单")
                                @RequestParam(required = false, defaultValue = "false") boolean onlyParent) {
        List<Option> menus = sysMenuService.listMenuOptions(onlyParent);
        return R.success(menus);
    }


    @Operation(summary = "获取菜单路由信息")
    @GetMapping("/getRouters")
    public R<List<RouterVo>> getRouters() {
        List<SysMenu> menus = sysMenuService.getRouters(SecurityUtils.getUserId());
        return R.success(sysMenuService.buildMenus(menus));
    }

    @Operation(summary = "获取菜单列表")
    @PreAuthorize("@ss.hasPermission('sys_menu_query')")
    @GetMapping("list")
    public R<List<SysMenu>> list(SysMenu menu) {
        List<SysMenu> menus = sysMenuService.selectMenuList(menu, SecurityUtils.getUserId());
        return R.success(menus);
    }


    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_menu_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysMenu>> page(@ParameterObject Page<SysMenu> page, @ParameterObject SysMenu sysMenu) {
        return R.success(sysMenuService.page(page, Wrappers.lambdaQuery(sysMenu)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_menu_get')")
    @GetMapping(value = "/{id}")
    public R<SysMenu> getById(@PathVariable Long id) {
        return R.success(sysMenuService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_menu_add')")
    @SysLog(title = "sys_menu", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysMenu sysMenu) {
        return sysMenuService.saveMenu(sysMenu) ? R.success() : R.failed();
    }


    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_menu_edit')")
    @SysLog(title = "sys_menu", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysMenu sysMenu) {
        return sysMenuService.saveMenu(sysMenu) ? R.success() : R.failed();
    }


    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_menu_del')")
    @SysLog(title = "sys_menu", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysMenuService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }

}
