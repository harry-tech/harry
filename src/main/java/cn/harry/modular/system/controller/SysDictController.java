package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.modular.system.domain.SysDict;
import cn.harry.modular.system.service.SysDictService;
import cn.harry.modular.system.vo.DictVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 字典管理
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "字典管理")
@RequestMapping("/sys/dict")
public class SysDictController {
    private final SysDictService sysDictService;

    @Operation(summary = "字典列表")
    @GetMapping("/list")
    public R<List<DictVO>> getAllDictWithData() {
        List<DictVO> list = sysDictService.getAllDictWithData();
        return R.success(list);
    }

    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_dict_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysDict>> page(@ParameterObject Page<SysDict> page, @ParameterObject SysDict sysDict) {
        return R.success(sysDictService.page(page, Wrappers.lambdaQuery(sysDict)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_dict_get')")
    @GetMapping(value = "/{id}")
    public R<SysDict> getById(@PathVariable Long id) {
        return R.success(sysDictService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_dict_add')")
    @SysLog(title = "sys_dict", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysDict sysDict) {
        return sysDictService.save(sysDict) ? R.success() : R.failed();
    }


    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_dict_edit')")
    @SysLog(title = "sys_dict", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysDict sysDict) {
        return sysDictService.updateById(sysDict) ? R.success() : R.failed();
    }


    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_dict_del')")
    @SysLog(title = "sys_dict", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysDictService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }

}
