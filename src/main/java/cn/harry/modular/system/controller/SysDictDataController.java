package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.modular.system.domain.SysDictData;
import cn.harry.modular.system.service.SysDictDataService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 字典数据
* @author harry
* @公众号 Harry技术
*/
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "字典数据")
@RequestMapping("/sys/dict/data")
public class SysDictDataController{
    private final SysDictDataService sysDictDataService;


    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_dict_data_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysDictData>> page(@ParameterObject Page<SysDictData> page, @ParameterObject  SysDictData sysDictData) {
        return R.success(sysDictDataService.page(page, Wrappers.lambdaQuery(sysDictData)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_dict_data_get')")
    @GetMapping(value = "/{id}")
    public R<SysDictData> getById(@PathVariable Long id) {
        return R.success(sysDictDataService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_dict_data_add')")
    @SysLog(title = "sys_dict", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysDictData sysDictData) {
        return sysDictDataService.save(sysDictData) ? R.success() : R.failed();
    }


    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_dict_data_edit')")
    @SysLog(title = "sys_dict", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Integer> update(@RequestBody SysDictData sysDictData) {
        return sysDictDataService.updateById(sysDictData) ? R.success() : R.failed();
    }


    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_dict_data_del')")
    @SysLog(title = "sys_dict", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysDictDataService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }

}
