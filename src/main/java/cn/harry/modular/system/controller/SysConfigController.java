package cn.harry.modular.system.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.constant.CacheConstants;
import cn.harry.common.enums.BusinessType;
import cn.harry.modular.system.domain.SysConfig;
import cn.harry.modular.system.service.SysConfigService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 配置信息
 *
* @author harry
* @公众号 Harry技术
*/
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "配置管理")
@RequestMapping("/sys/config")
public class SysConfigController{
    private final SysConfigService sysConfigService;


    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_config_page')")
    @GetMapping(value = "/page")
    public R<IPage<SysConfig>> page(@ParameterObject Page<SysConfig> page, @ParameterObject  SysConfig sysConfig) {
        return R.success(sysConfigService.page(page, Wrappers.lambdaQuery(sysConfig)));
    }


    @Operation(summary = "详情")
    @PreAuthorize("@ss.hasPermission('sys_config_get')")
    @GetMapping(value = "/{id}")
    public R<SysConfig> getById(@PathVariable Long id) {
        return R.success(sysConfigService.getById(id));
    }


    @Operation(summary = "新增")
    @PreAuthorize("@ss.hasPermission('sys_config_add')")
    @SysLog(title = "sys_config", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Integer> save(@RequestBody SysConfig sysConfig) {
        return sysConfigService.save(sysConfig) ? R.success() : R.failed();
    }


    @Operation(summary = "更新")
    @PreAuthorize("@ss.hasPermission('sys_config_edit')")
    @SysLog(title = "sys_config", businessType = BusinessType.UPDATE)
    @PutMapping
    @CacheEvict(value = CacheConstants.CONFIG_DETAILS, key = "#sysConfig.configKey")
    public R<Integer> update(@RequestBody SysConfig sysConfig) {
        return sysConfigService.updateById(sysConfig) ? R.success() : R.failed();
    }


    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_config_del')")
    @SysLog(title = "sys_config", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    @CacheEvict(value = CacheConstants.CONFIG_DETAILS, allEntries = true)
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysConfigService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }


    @Operation(summary = "refreshCache 刷新参数缓存")
    @PreAuthorize("@ss.hasPermission('system_config_remove')")
    @SysLog(title = "系统配置", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/refreshCache")
    @CacheEvict(value = CacheConstants.CONFIG_DETAILS, allEntries = true)
    public R<Integer> refreshCache() {
        return R.success();
    }

}
