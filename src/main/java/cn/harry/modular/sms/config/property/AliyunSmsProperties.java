package cn.harry.modular.sms.config.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Configuration
@ConfigurationProperties(prefix = "sms.aliyun")
@Data
public class AliyunSmsProperties {
    /**
     * 是否启用
     */
    private Boolean enable;
    /**
     * 阿里云服务的区域ID，如cn-shanghai
     */
    private String regionId;
    /**
     * 阿里云账户的Access Key ID，用于API请求认证
     */
    private String accessKeyId;

    /**
     * 阿里云账户的Access Key Secret，用于API请求认证
     */
    private String accessKeySecret;
    /**
     * 短信签名，必须是已经在阿里云短信服务中注册并通过审核的
     */
    private String signName;
    /**
     * 模板编码
     */
    private Map<String, String> templateCodes;
}
