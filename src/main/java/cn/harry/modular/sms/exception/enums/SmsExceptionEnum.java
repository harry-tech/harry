package cn.harry.modular.sms.exception.enums;

import cn.harry.common.api.IErrorCode;

/**
 * @author harry
 * @公众号 Harry技术
 */
public enum SmsExceptionEnum implements IErrorCode {

    /******** 短信模块 sys_sms 1501 *********/
    MESSAGE_TO_MANY(1501, "1分钟之内不能重发2次"),
    MSG_TYPE_ERROR(1502, "短信类型错误"),
    MSG_CODE_ERROR(1503, "验证码错误"),
    MOBILE_ERROR(1504, "手机号有误"),
    PHONE_IS_EMPTY(1505, "手机号为空"),
    VERIFICATION_CODE_FAILED(1506, "验证码验证失败"),
    TEMPLATE_CODE_ERROR(1507, "短信模版未配置"),
    MSG_PARAM_ERROR(1508, "短信参数错误"),

    ;
    private final long code;

    private final String msg;

    SmsExceptionEnum(long code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return msg;
    }
}
