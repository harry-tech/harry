package cn.harry.modular.sms.param;

import cn.harry.common.exception.ApiException;
import cn.harry.modular.sms.exception.enums.SmsExceptionEnum;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class SmsParam {

    /**
     * 【必填】 发送短信手机号码集合
     */
    private List<String> mobileList;

    /**
     * [非必填] 阿里大鱼的参数集合
     */
    private Map<String, String> params;

    /**
     * 短信模板
     */
    private String templateCode;

    private SmsParam() {

    }

    private SmsParam(List<String> mobileList, Map<String, String> params,
                     String templateKey) {
        this.mobileList = mobileList;
        this.params = params;
        this.templateCode = templateKey;
    }

    public static SmsParam.SmsParamBuilder builder() {
        return new SmsParam.SmsParamBuilder();
    }

    public static class SmsParamBuilder {

        /**
         * 获取手机号
         */
        private final List<String> mobiles = new ArrayList<>();

        /**
         * 参数
         */
        private final Map<String, String> params = new HashMap<>();

        /**
         * 模版
         */
        private String templateCode;

        public SmsParam.SmsParamBuilder mobile(String mobile) {
            this.mobiles.add(mobile);
            return this;
        }

        public SmsParam.SmsParamBuilder mobiles(List<String> mobiles) {
            this.mobiles.addAll(mobiles);
            return this;
        }

        public SmsParam.SmsParamBuilder param(String key, String value) {
            this.params.put(key, value);
            return this;
        }

        public SmsParam.SmsParamBuilder params(Map<String, String> params) {
            this.params.putAll(params);
            return this;
        }


        public SmsParam.SmsParamBuilder templateCode(String templateCode) {
            this.templateCode = templateCode;
            return this;
        }

        public SmsParam build() {
            if (this.templateCode == null) {
                throw new ApiException(SmsExceptionEnum.TEMPLATE_CODE_ERROR);
            }

            if (CollectionUtils.isEmpty(mobiles)) {
                throw new ApiException(SmsExceptionEnum.MOBILE_ERROR);
            }
            if (CollectionUtils.isEmpty(params)) {
                throw new ApiException(SmsExceptionEnum.MSG_PARAM_ERROR);
            }
            return new SmsParam(this.mobiles, this.params, this.templateCode);
        }

    }

}
