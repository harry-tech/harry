package cn.harry.modular.sms.service.impl;

import cn.harry.modular.sms.param.SmsParam;
import cn.harry.modular.sms.service.SmsStrategy;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * @author harry
 * @公众号 Harry技术
 */

@Slf4j
@Component
@ConditionalOnProperty(value = "sms.aliyun.enable", havingValue = "false")
public class FakeSmsStrategy implements SmsStrategy {
    @Override
    public SendSmsResponse send(SmsParam smsParam) {
        log.info("FakeSmsStrategy smsParam:{}", JSONUtil.toJsonStr(smsParam));
        String str = "{\"bizId\":\"111111111\",\"code\":\"OK\",\"message\":\"并未真实发送\",\"requestId\":\"111111111\"}";
        return BeanUtil.copyProperties(JSONUtil.parseObj(str), SendSmsResponse.class);
    }
}
