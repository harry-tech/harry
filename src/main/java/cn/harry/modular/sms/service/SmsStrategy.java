package cn.harry.modular.sms.service;

import cn.harry.modular.sms.param.SmsParam;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;

/**
 * 短信发送策略
 *
 * @author harry
 * @公众号 Harry技术
 */
public interface SmsStrategy {

    SendSmsResponse send(SmsParam smsParam);
}
