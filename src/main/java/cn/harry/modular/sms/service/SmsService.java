package cn.harry.modular.sms.service;

import cn.harry.modular.sms.param.SmsParam;

/**
 * @author harry
 * @公众号 Harry技术
 */
public interface SmsService {

    /**
     * 发送短信验证码
     *
     * @param mobile 手机号
     * @param code 验证码
     * @return
     */
    String sendSmsCode(String mobile, String templateCode, String code);

    /**
     * desc 发送短信
     *
     * @param smsParam  短信模板 短信参数
     */
    Boolean sendSms(SmsParam smsParam);
}
