package cn.harry.modular.auth.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 用户登录参数
 *
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class SysUserLoginParam {

    /**
     * 用户
     */
    @Schema(description = "用户名")
    private String username;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 图形验证码
     */
    @Schema(description = "验证码")
    private String code;

    /**
     * 唯一标识
     */
    @Schema(description = "唯一标识")
    private String uuid = "";

}
