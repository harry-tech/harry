package cn.harry.modular.auth.controller;

import cn.harry.common.api.R;
import cn.harry.modular.auth.param.SysUserLoginParam;
import cn.harry.modular.auth.vo.LoginResult;
import cn.harry.modular.auth.vo.UserInfoResult;
import cn.harry.modular.system.enums.ConfigEnums;
import cn.harry.modular.system.service.SysCaptchaService;
import cn.harry.modular.system.service.SysUserService;
import cn.harry.modular.system.utils.ParamResolver;
import cn.harry.modular.system.vo.CaptchaResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * 登录相关 控制器
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@Tag(name = "认证中心")
@RequestMapping("/auth")
public class LoginController {

    private final SysUserService sysUserService;
    private final SysCaptchaService sysCaptchaService;

    @Operation(summary = "captcha 生成验证码")
    @GetMapping("/captcha")
    public R<CaptchaResult> captcha() {
        CaptchaResult result = sysCaptchaService.getCaptcha();
        // 验证码开关
        boolean sw = Boolean.parseBoolean(ParamResolver.getStr(ConfigEnums.SYS_CAPTCHA_IMG.name(), "false"));
        result.setEnabled(sw);
        return R.success(result);
    }

    @Operation(summary = "login 登录")
    @PostMapping(value = "/login")
    public R<LoginResult> login(@RequestBody SysUserLoginParam sysUserLoginParam) {
        // 获取系统验证码开关
        boolean sw = Boolean.parseBoolean(ParamResolver.getStr(ConfigEnums.SYS_CAPTCHA_IMG.name(), "false"));
        if (sw) {
            // 验证码校验
            boolean captcha = sysCaptchaService.validate(sysUserLoginParam.getUuid(), sysUserLoginParam.getCode());
            if (!captcha) {
                return R.failed("验证码不正确");
            }
        }
        return R.success(sysUserService.login(sysUserLoginParam.getUsername(), sysUserLoginParam.getPassword()));
    }

    @Operation(summary = "info 获取当前用户信息")
    @GetMapping(value = "/info")
    public R<UserInfoResult> getInfo() {
        UserInfoResult result = sysUserService.getInfo();
        return R.success(result);
    }

    @Operation(summary = "logout 注销")
    @PostMapping(value = "/logout")
    public R logout(HttpServletRequest request) {
        // 需要 将当前用户token 设置无效
        SecurityContextHolder.clearContext();
        return R.success();
    }

}
