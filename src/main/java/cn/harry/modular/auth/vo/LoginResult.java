package cn.harry.modular.auth.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 登录返回结果
 *
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class LoginResult {

    @Schema(description = "token")
    private String token;

    @Schema(description = "token 类型", example = "Bearer")
    private String tokenType;

    @Schema(description = "过期时间(单位：秒)", example = "604800")
    private Long expiration;

    @Schema(description = "刷新token")
    private String refreshToken;

}
