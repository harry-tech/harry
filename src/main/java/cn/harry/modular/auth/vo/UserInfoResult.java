package cn.harry.modular.auth.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Set;

/**
 * 鉴权后的用户信息
 *
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class UserInfoResult {

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "用户详细信息")
    private SysUserInfo userInfo;

    @Schema(description = "权限集")
    private Set<String> permissions;

    @Schema(description = "角色集")
    private Set<String> roles;

}
