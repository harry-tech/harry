package cn.harry.modular.auth.vo;

import cn.hutool.core.date.DatePattern;
import cn.hutool.json.JSONArray;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息
 *
 * @author harry
 * @公众号 Harry技术
 */
@Data
public class SysUserInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "用户ID")
    private Long id;

    /**
     * 所属部门
     */
    @Schema(description = "所属部门")
    private Long deptId;

    /**
     * 所属部门名称
     */
    @Schema(description = "所属部门名称")
    private String deptName;

    /**
     * 所属岗位
     */
    @Schema(description = "所属岗位")
    private JSONArray postIds;

    /**
     * 登陆用户名
     */
    @Schema(description = "登陆用户名")
    private String username;

    /**
     * 头像
     */
    @Schema(description = "头像")
    private String icon;

    /**
     * 邮箱
     */
    @Schema(description = "邮箱")
    private String email;

    /**
     * 手机号码
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 昵称
     */
    @Schema(description = "昵称")
    private String nickName;

    /**
     * 性别
     */
    @Schema(description = "性别")
    private String sex;

    /**
     * 备注信息
     */
    @Schema(description = "备注信息")
    private String note;

    /**
     * 最后登陆IP
     */
    @Schema(description = "最后登陆IP")
    private String loginIp;

    /**
     * 最后登录时间
     */
    @Schema(description = "最后登录时间")
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date loginTime;

    /**
     * 启用状态，0:禁用 1:启用
     */
    @Schema(description = "启用状态，0:禁用 1:启用")
    private String status;

}
