package cn.harry.modular.monitor.mapper;

import cn.harry.modular.monitor.domain.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志Mapper接口
 *
 * @author harry
 * @公众号 Harry技术
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {

}
