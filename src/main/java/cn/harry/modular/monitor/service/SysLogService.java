package cn.harry.modular.monitor.service;

import cn.harry.modular.monitor.domain.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统日志服务类
 *
 * @author harry
 * @公众号 Harry技术
 */
public interface SysLogService extends IService<SysLog> {

}
