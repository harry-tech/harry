package cn.harry.modular.monitor.service.impl;

import cn.harry.modular.monitor.domain.SysLog;
import cn.harry.modular.monitor.mapper.SysLogMapper;
import cn.harry.modular.monitor.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 系统日志服务实现类
 *
 * @author harry
 * @公众号 Harry技术
 */
@Service
@RequiredArgsConstructor
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

}
