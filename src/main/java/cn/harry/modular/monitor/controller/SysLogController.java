package cn.harry.modular.monitor.controller;

import cn.harry.common.annotation.SysLog;
import cn.harry.common.api.R;
import cn.harry.common.enums.BusinessType;
import cn.harry.modular.monitor.service.SysLogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 系统日志前端控制层
 *
 * @author harry
 * @公众号 Harry技术
 */
@Tag(name = "系统日志接口")
@RestController
@RequestMapping("/monitor/log")
@RequiredArgsConstructor
public class SysLogController {

    private final SysLogService sysLogService;

    /**
     * 分页查询列表
     */
    @Operation(summary = "分页查询")
    @PreAuthorize("@ss.hasPermission('sys_log_page')")
    @GetMapping(value = "/page")
    public R<IPage<cn.harry.modular.monitor.domain.SysLog>> page(@ParameterObject Page<cn.harry.modular.monitor.domain.SysLog> page, @ParameterObject cn.harry.modular.monitor.domain.SysLog entity) {
        return R.success(sysLogService.page(page, Wrappers.lambdaQuery(entity)));
    }

    /**
     * 根据id获取详情
     */
    @Operation(summary = "根据id获取详情")
    @PreAuthorize("@ss.hasPermission('sys_log_get')")
    @GetMapping(value = "/{id}")
    public R<cn.harry.modular.monitor.domain.SysLog> getById(@PathVariable Long id) {
        return R.success(sysLogService.getById(id));
    }

    /**
     * 删除
     */
    @Operation(summary = "删除")
    @PreAuthorize("@ss.hasPermission('sys_log_del')")
    @SysLog(title = "sys_log", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public R<Integer> deleteByIds(@Parameter(description = "多个以英文逗号(,)拼接") @PathVariable Long[] ids) {
        return sysLogService.removeBatchByIds(Arrays.asList(ids)) ? R.success() : R.failed();
    }
}
