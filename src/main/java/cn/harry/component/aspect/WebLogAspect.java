package cn.harry.component.aspect;

import cn.harry.common.enums.HttpMethod;
import cn.harry.common.manager.AsyncManager;
import cn.harry.common.manager.factory.AsyncFactory;
import cn.harry.common.utils.IpUtil;
import cn.harry.common.utils.ServletUtils;
import cn.harry.component.security.utils.SecurityUtils;
import cn.harry.modular.monitor.domain.SysLog;
import cn.harry.modular.system.domain.SysUser;
import cn.harry.modular.system.enums.StatusEnums;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import cn.hutool.json.JSONUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * 日志切面
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
@Aspect
@Component
public class WebLogAspect {

    /**
     * 线程绑定变量，用于记录请求的开始时间
     */
    private static final ThreadLocal<Long> START_TIME_THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 配置织入点
     */
    @Pointcut("@annotation(cn.harry.common.annotation.SysLog)")
    public void logPointCut() {

    }

    @Before("logPointCut()")
    public void recordStartTime(JoinPoint joinPoint) {
        START_TIME_THREAD_LOCAL.set(System.currentTimeMillis());
    }

    /**
     * 处理完请求后执行
     *
     * @param joinPoint 切点
     */
    @AfterReturning(pointcut = "logPointCut()", returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, Object jsonResult) {
        long time = System.currentTimeMillis() - START_TIME_THREAD_LOCAL.get();
        handleLog(joinPoint, null, jsonResult, time);
        START_TIME_THREAD_LOCAL.remove();

    }

    /**
     * 拦截异常操作
     *
     * @param joinPoint 切点
     * @param e         异常
     */
    @AfterThrowing(value = "logPointCut()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Exception e) {
        long time = System.currentTimeMillis() - START_TIME_THREAD_LOCAL.get();
        handleLog(joinPoint, e, null, time);
        START_TIME_THREAD_LOCAL.remove();
    }

    protected void handleLog(final JoinPoint joinPoint, final Exception e, Object jsonResult, Long executionTime) {
        try {
            // 获得注解
            cn.harry.common.annotation.SysLog log = getAnnotationLog(joinPoint);
            if (log == null) {
                return;
            }

            // 获取当前的用户
            SysUser loginUser = SecurityUtils.getSysUserIsNull();

            // *========数据库日志=========*//
            SysLog sysLog = new SysLog();
            sysLog.setStatus(StatusEnums.ENABLE.getKey());

            HttpServletRequest request = ServletUtils.getRequest();
            // 请求的IP地址
            String ip = IpUtil.getIp(request);
            sysLog.setIp(ip);
            // 返回参数
            sysLog.setJsonResult(JSONUtil.toJsonStr(jsonResult));
            // 请求URL
            sysLog.setUrl(request.getRequestURI());

            if (loginUser != null) {
                sysLog.setUsername(loginUser.getUsername());
            }

            if (e != null) {
                sysLog.setStatus(StatusEnums.DISABLE.getKey());
                sysLog.setErrorMsg(StrUtil.sub(e.getMessage(), 0, 2000));
            }
            // 设置方法名称
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = joinPoint.getSignature().getName();
            sysLog.setMethod(className + "." + methodName + "()");
            // 设置请求方式
            sysLog.setRequestMethod(ServletUtils.getRequest().getMethod());
            sysLog.setCreateTime(DateUtil.date());


            // 获取浏览器和终端系统信息
            String userAgentString = request.getHeader("User-Agent");
            UserAgent userAgent = UserAgentUtil.parse(userAgentString);
            // 系统信息
            sysLog.setOs(userAgent.getOs().getName());
            // 浏览器信息
            sysLog.setBrowser(userAgent.getBrowser().getName());
            sysLog.setBrowserVersion(userAgent.getBrowser().getVersion(userAgentString));

            // 处理设置注解上的参数
            getControllerMethodDescription(joinPoint, log, sysLog);

            // 执行时长
            sysLog.setExecutionTime(executionTime);

            // 保存数据库
            AsyncManager.me().execute(AsyncFactory.logTask(sysLog));
        } catch (Exception exp) {
            // 记录本地异常日志
            log.error("==前置通知异常==");
            log.error("异常信息:{}", exp.getMessage());
        }
    }

    /**
     * 获取注解中对方法的描述信息 用于Controller层注解
     *
     * @param log    日志
     * @param sysLog 操作日志
     */
    public void getControllerMethodDescription(JoinPoint joinPoint, cn.harry.common.annotation.SysLog log, SysLog sysLog) throws Exception {
        // 设置标题
        sysLog.setTitle(log.title());
        // 是否需要保存request，参数和值
        if (log.isSaveRequestData()) {
            // 获取参数的信息，传入到数据库中。
            setRequestValue(joinPoint, sysLog);
        }
    }

    /**
     * 获取请求的参数，放到log中
     *
     * @param sysLog 操作日志
     * @throws Exception 异常
     */
    private void setRequestValue(JoinPoint joinPoint, SysLog sysLog) throws Exception {
        String requestMethod = sysLog.getRequestMethod();
        if (HttpMethod.PUT.name().equals(requestMethod) || HttpMethod.POST.name().equals(requestMethod)) {
            String params = argsArrayToString(joinPoint.getArgs());
            sysLog.setParam(StrUtil.sub(params, 0, 2000));
        } else {
            Map<?, ?> paramsMap = (Map<?, ?>) ServletUtils.getRequest().getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
            sysLog.setParam(StrUtil.sub(paramsMap.toString(), 0, 2000));
        }
    }

    /**
     * 是否存在注解，如果存在就获取
     */
    private cn.harry.common.annotation.SysLog getAnnotationLog(JoinPoint joinPoint) throws Exception {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(cn.harry.common.annotation.SysLog.class);
        }
        return null;
    }

    /**
     * 参数拼装
     */
    private String argsArrayToString(Object[] paramsArray) {
        StringBuilder params = new StringBuilder();
        if (paramsArray != null) {
            for (Object o : paramsArray) {
                if (!isFilterObject(o)) {
                    Object jsonObj = JSONUtil.toJsonStr(o);
                    params.append(jsonObj.toString()).append(" ");
                }
            }
        }
        return params.toString().trim();
    }

    /**
     * 判断是否需要过滤的对象。
     *
     * @param o 对象信息。
     * @return 如果是需要过滤的对象，则返回true；否则返回false。
     */
    public boolean isFilterObject(final Object o) {
        return o instanceof MultipartFile || o instanceof HttpServletRequest || o instanceof HttpServletResponse;
    }

}
