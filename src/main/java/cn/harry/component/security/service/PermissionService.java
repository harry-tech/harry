package cn.harry.component.security.service;

import cn.harry.common.constant.CommonConstant;
import cn.harry.common.utils.StringUtils;
import cn.harry.component.security.utils.SecurityUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 权限标识服务
 *
 * @author harry
 * @公众号 Harry技术
 */
@Service("ss")
public class PermissionService {

    public Boolean hasPermission(String... permissions) {
        if (StringUtils.isEmpty(permissions)) {
            return false;
        }
        // 获取当前用户的所有权限
        List<String> perms = SecurityUtils.getUserDetails().getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();
        // 判断当前用户的所有权限是否包含接口上定义的权限
        return perms.contains(CommonConstant.ALL_PERMISSION) || Arrays.stream(permissions).anyMatch(perms::contains);
    }

}
