package cn.harry.component.security.constant;

/**
 * Security 常量
 *
 * @author harry
 * @公众号 Harry技术
 */
public interface SecurityConstants {


    /**
     * JWT Token 前缀
     */
    String JWT_TOKEN_PREFIX = "Bearer ";


}
