package cn.harry.component.security.utils;

import cn.harry.common.api.ResultCode;
import cn.harry.common.constant.CommonConstant;
import cn.harry.common.exception.ApiException;
import cn.harry.component.security.model.SysUserDetails;
import cn.harry.modular.system.domain.SysUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Set;


/**
 * 安全服务工具类
 *
 * @author harry
 * @公众号 Harry技术
 */
public class SecurityUtils {


    public static UserDetails getUserDetails() {
        try {
            return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new ApiException(ResultCode.UNAUTHORIZED);
        }
    }

    public static SysUserDetails getSysUserDetails() {
        try {
            return (SysUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new ApiException(ResultCode.UNAUTHORIZED);
        }
    }

    /**
     * 是否超级管理员
     * <p>
     * 超级管理员忽视任何权限判断
     */
    public static boolean isRoot() {
        Set<String> roles = getRoles();
        return roles.contains(CommonConstant.SUPER_ADMIN_ROOT);
    }

    /**
     * 获取当前 用户id
     *
     * @return 用户id
     */
    public static Long getUserId() {
        return getSysUser().getId();
    }


    public static Set<String> getRoles() {
        return getSysUserDetails().getRoles();
    }

    public static SysUser getSysUser() {
        try {
            SecurityContext ctx = SecurityContextHolder.getContext();
            Authentication auth = ctx.getAuthentication();
            SysUserDetails sysUserDetails = (SysUserDetails) auth.getPrincipal();
            return sysUserDetails.getSysUser();
        } catch (Exception e) {
            throw new ApiException(ResultCode.UNAUTHORIZED);
        }
    }

    public static SysUser getSysUserIsNull() {
        try {
            SecurityContext ctx = SecurityContextHolder.getContext();
            Authentication auth = ctx.getAuthentication();
            SysUserDetails sysUserDetails = (SysUserDetails) auth.getPrincipal();
            return sysUserDetails.getSysUser();
        } catch (Exception e) {
           return null;
        }
    }

    public static Integer getDataScope() {
        return  getSysUserDetails().getDataScope();
    }

    public static Long getDeptId() {
        return getSysUserDetails().getSysUser().getDeptId();
    }
}
