package cn.harry.common.enums;

import cn.harry.common.base.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 数据权限枚举
 *
 * @author harry
 * @公众号 Harry技术
 */
@Getter
@AllArgsConstructor
public enum DataScopeEnums implements IBaseEnum<Integer> {
    /**
     * value 越小，数据权限范围越大
     */
    ALL(0, "所有数据权限"),
    DEPT_AND_SUB(1, "本部门及子部门数据"),
    DEPT(2, "本部门数据权限"),
    SELF(3, "本人数据权限"),
    ;
    private final Integer value;

    private final String label;

}
