package cn.harry.common.constant;

/**
 * 缓存的key 常量
 *
 * @author harry
 * @公众号 Harry技术
 */
public interface CacheConstants {

    /**
     * 菜单信息缓存
     */
    String MENU_DETAILS = "menu_details";

    /**
     * 用户信息缓存
     */
    String USER_DETAILS = "user_details";

    /**
     * 字典信息缓存
     */
    String DICT_DETAILS = "dict_details";

    /**
     * 参数缓存
     */
    String CONFIG_DETAILS = "config_details";
    /**
     * 图形验证码缓存
     */
    String CAPTCHA_CODE = "captcha_code:";
    /**
     * 短信验证码缓存
     */
    String SMS_CODE = "sms_code:";
    /**
     * 邮箱验证码缓存
     */
    String EMAIL_CODE = "email_code:";

}
