package cn.harry.common.constant;

/**
 * 公共常量
 *
 * @author harry
 * @公众号 Harry技术
 */
public interface CommonConstant {
    /**
     * 超级管理员角色
     */
    String SUPER_ADMIN_ROOT = "ROOT";

    /**
     * 所有权限标识
     */
    String ALL_PERMISSION = "*:*:*";

    /**
     * 根节点ID
     */
    Long ROOT_NODE_ID = 0L;

}
