package cn.harry.common.utils;

import cn.hutool.core.io.IoUtil;
import cn.hutool.http.useragent.Browser;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.lionsoul.ip2region.xdb.Searcher;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

/**
 * IP地址工具类定义
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
public class IpUtil {

    /**
     * 用于IP定位转换
     */
    private static final String REGION = "内网IP|内网IP";

    private static final String UNKNOWN = "unknown";

    public static String getIp() {
        return getIp(ServletUtils.getRequest());
    }

    public static String getIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.isEmpty() || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        String comma = ",";
        String localhost = "127.0.0.1";
        if (ip.contains(comma)) {
            ip = ip.split(",")[0];
        }
        if (localhost.equals(ip)) {
            // 获取本机真正的ip地址
            try {
                ip = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        log.info("getIpAddr:{} ", ip);
        return ip;
    }

    /**
     * 根据ip获取详细地址
     */
    public static String getAddress(String ip) {
        // 1、创建 searcher 对象
        String dbPath = "ip2region/ip2region.xdb";
        // 获取ip2region.db文件的位置
        InputStream inputStream = IpUtil.class.getClassLoader()
                .getResourceAsStream(dbPath);
        Searcher searcher = null;
        try {
            if ("0:0:0:0:0:0:0:1".equals(ip)) {
                return REGION;
            }

            long sTime = System.nanoTime();
            searcher = Searcher.newWithBuffer(IoUtil.readBytes(inputStream));
            // 2、查询
            String region = searcher.search(ip);
            long cost = TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - sTime);
            log.info("region: {}, ioCount: {}, took: {} μs", region,
                    searcher.getIOCount(), cost);
            return region;
        } catch (Exception e) {
            log.error("failed to create searcher with {}", dbPath, e);
        } finally {
            if (searcher != null) {
                try {
                    searcher.close();
                } catch (IOException ignored) {

                }
            }
        }
        return REGION;
    }

    /**
     * 获取浏览器类型 浏览器信息
     *
     * @param request
     * @return
     */
    public static String getBrowser(HttpServletRequest request) {
        // 获取浏览器和终端系统信息
        String userAgentString = request.getHeader("User-Agent");
        UserAgent userAgent = UserAgentUtil.parse(userAgentString);
        Browser browser = userAgent.getBrowser();
        return browser.getName();
    }

    /**
     * 操作系统 系统信息
     */
    public static String getOs(HttpServletRequest request) {
        // 获取浏览器和终端系统信息
        String userAgentString = request.getHeader("User-Agent");
        UserAgent userAgent = UserAgentUtil.parse(userAgentString);
        return userAgent.getOs().getName();
    }

}
