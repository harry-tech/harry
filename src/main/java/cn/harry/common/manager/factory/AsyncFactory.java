package cn.harry.common.manager.factory;

import cn.harry.common.utils.IpUtil;
import cn.harry.common.utils.SpringUtils;
import cn.harry.modular.monitor.domain.SysLog;
import cn.harry.modular.monitor.service.SysLogService;
import lombok.extern.slf4j.Slf4j;

import java.util.TimerTask;

/**
 * 异步工厂（产生任务用）
 *
 * @author harry
 * @公众号 Harry技术
 */
@Slf4j
public class AsyncFactory {

    /**
     * 操作日志记录
     *
     * @param sysLog 操作日志信息
     * @return 任务task
     */
    public static TimerTask logTask(final SysLog sysLog) {
        return new TimerTask() {
            @Override
            public void run() {
                String address = IpUtil.getAddress(sysLog.getIp());
                // 远程查询操作地点
                sysLog.setLocation(address);

                SpringUtils.getBean(SysLogService.class).save(sysLog);
            }
        };
    }

}
